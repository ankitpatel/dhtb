﻿package tilegame
{
	import citrus.core.CitrusEngine;
	import citrus.core.starling.StarlingCitrusEngine;
	import citrus.core.starling.ViewportMode;
	import citrus.view.starlingview.StarlingCamera;
	
	import com.gestouch.core.Gestouch;
	import com.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import com.gestouch.extensions.starling.StarlingTouchHitTester;
	import com.gestouch.input.NativeInputAdapter;
	
	import flash.display.LoaderInfo;
	import flash.display.StageAlign;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DProfile;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	
	import tilegame.components.LocalStorage;
	import tilegame.components.ScoreBar;
	import tilegame.star.Assets;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;
	import tilegame.states.ScoreState;
	import tilegame.states.SplashState;
	
	[SWF(width="600", height="800", frameRate="60", backgroundColor="#168D63")]
	public class CitrusGame extends StarlingCitrusEngine
	{
		public static var _camera:StarlingCamera;
		public static const ASSETS_LOADED:String = "ASSETS_LOADED";
		
		private var paramObj:*;
		
		
		public function CitrusGame()
		{
			trace("CitrusGame.CitrusGame()");
			initGame();
			
			//init facebook api//
			//init();
		}
		
		///////////////facebook integration////////////////////////////
		/*
		public function init():void{
			
				ExternalInterface.addCallback("myFlashcall",myFlashcall);
				Constants.stage.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		
		public function myFlashcall(str:String):void{
			trace("myFlashcall: "+str);
		}
		
		public function onClick(event:MouseEvent):void{
			if(ExternalInterface.available){
				trace("onClick");
				ExternalInterface.call("myFBcall");
			}
		}
		*/
		///////////////////////////////////////////
		///////////////////////////////////////////
		
		override protected function handleAddedToStage(e:Event):void
		{
			super.handleAddedToStage(e);
			
			//init starling 
			trace("??",Constants.SCREEN_WIDTH);
			if(Constants.SCREEN_WIDTH >= 2048)
			{
				setUpStarling(false,1,Constants.VIEWPORT,Context3DProfile.BASELINE);
			}
			else
			{
				setUpStarling(false,1,Constants.VIEWPORT,Context3DProfile.BASELINE);
			}
			
		}
		
		
		override public function handleStarlingReady():void
		{
			//test();
			Starling.current.shareContext = false; //set it false
			Starling.current.stage.stageWidth = Constants.DEVICE_WIDTH;
			Starling.current.stage.stageHeight = Constants.DEVICE_HEIGHT;
			if(!starHelper.stageRef)starHelper.stageRef = Starling.current.stage;
			
			Gestouch.inputAdapter ||= new NativeInputAdapter(Constants.stage);
			Gestouch.addDisplayListAdapter(DisplayObject, new StarlingDisplayListAdapter());
			Gestouch.addTouchHitTester(new StarlingTouchHitTester(Starling.current), -1);			
			
			
			trace("CitrusGame.handleStarlingReady()",starHelper.stageRef);
			
			loadAssets();
		}
		
		public function initGame(gameConstants:Object=null) 
		{
			trace("CitrusGame.initGame(gameConstants)");
			
			var oriScreenHeight:Number = Math.max(Capabilities.screenResolutionX,Capabilities.screenResolutionY);
			var oriScreenWidth:Number = Math.min(Capabilities.screenResolutionX,Capabilities.screenResolutionY);
			
			paramObj = LoaderInfo(this.root.loaderInfo).parameters;   //set the paramObj variable to the parameters property of the LoaderInfo object
			if(!Constants.isAndroid && !Constants.isIOS)
			{
				oriScreenWidth = 600;
				oriScreenHeight = 800;
			}
			if(paramObj.hasOwnProperty("screenWidth"))
			{
				oriScreenWidth = paramObj["screenWidth"];
				oriScreenHeight = paramObj["screenHeight"];
			}
			
			
			Constants.SCREEN_WIDTH = oriScreenWidth;
			Constants.SCREEN_HEIGHT = oriScreenHeight;
			
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			Constants.stageRef = this;
			Constants.stage = this.stage;
			Constants.stage.color = Constants.DARK_COLOR;
			
			setPlayerConfig(Constants.EASY);
			setPlayerConfig(Constants.MEDIUM);
			setPlayerConfig(Constants.HARD);
			
			Constants.citrusRef = CitrusEngine.getInstance();
			Constants.ASPECT_RATIO = '4x3';
			setResolution(Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT);
			
		} 
		
		private function setPlayerConfig(key:String):void
		{
			LocalStorage.PLAYER_KEY = key;
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,key);
			if(!localStorageObj){
				localStorageObj = new Object();
				localStorageObj.bestscore = 0;
				LocalStorage.addObject(LocalStorage.PLAYER_DATA,key,localStorageObj);
				
			}
			
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.ATTEMPT_KEY);
			if(!localStorageObj){
				localStorageObj = new Object();
				localStorageObj.attempts = 0;
				LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.ATTEMPT_KEY,localStorageObj);
				
			}
		}
		
		
		
		private function onAssetsLoaded(e:Event=null):void
		{
			trace("CitrusGame.onAssetsLoaded(e), COINS");
			if(!Constants.citrusRef)Constants.citrusRef = CitrusEngine.getInstance();
			
			
			AppHelper.init();
			Constants.citrusRef.state = new SplashState();
			//Constants.citrusRef.state = new GameState(true);
			
		}
		
		
		
		private static function dispatchLoadCompleteEvent(e:*=null):void
		{
			trace("CitrusGame.dispatchLoadCompleteEvent(e)");
			
			import flash.events.Event;
			if(Constants.stage){
				Constants.stage.dispatchEvent(new flash.events.Event(ASSETS_LOADED));
			}
			
		}
		
		
		private function loadAssets(e:*=null):void
		{
			trace("CitrusGame.loadAssets(e)");
			Assets.init(onAssetsLoaded);
			
		}
		
		
		
		public function setResolution(screenWidth:*=null,screenHeight:*=null):void
		{
			
			Constants.stageRef.scaleX = screenWidth / 768;
			Constants.stageRef.scaleY = screenHeight / 1024;
			Constants.DEVICE_WIDTH = 768;
			Constants.DEVICE_HEIGHT = 1024;
			
			Constants.VIEWPORT =  new Rectangle(0, 0, screenWidth, screenHeight);
			Constants.stage.quality = StageQuality.HIGH;
			
			_baseWidth = Constants.DEVICE_WIDTH;
			_baseHeight = Constants.DEVICE_HEIGHT;
			if(paramObj.hasOwnProperty("screenWidth"))
			{
				_viewportMode = ViewportMode.LETTERBOX;
			}
			else
			{
				_viewportMode = ViewportMode.MANUAL;
			}
			
		}
	}
}