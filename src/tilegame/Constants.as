﻿package tilegame
{
	import citrus.core.CitrusEngine;
	import citrus.core.starling.StarlingState;
	
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import lib.rateus.RateBoxExample;
	
	import starling.core.Starling;
	
	import tilegame.components.PlayerIcon;
	import tilegame.helpers.AdViewerHelper;

	/**
	 * ...
	 * @author ankit
	 */
	public class Constants 
	{
		public static var CURRENT_VERSION:String = '1.1';
		
		public static const EASY:String = "NORMAL";
		public static const MEDIUM:String = "FASTER";
		public static const HARD:String = "PRO";
		
		//mute flag
		public static var musicFlag:Boolean = true;
		public static var soundFlag:Boolean = true;
		
		public static var ratingBox:RateBoxExample;
		public static var adViewHelper:AdViewerHelper;
		
		
		//platfrom constants
		static public var stage:Stage;
		public static var stageRef:MovieClip;
		public static var starlingRef:Starling;
		static public var ROOT_PATH:String="";  
		public static var DEVICE_WIDTH:Number;
        public static var DEVICE_HEIGHT:Number;
		static public var SCREEN_WIDTH:Number;
		static public var SCREEN_HEIGHT:Number;
		static public var VIEWPORT:Rectangle;
		public static var XOFFSET:Number=0;
		static public var BROWAER_XOFFSET:Number=0;
		static public var DEVICE:String;
		static public const IPAD:String = "ipad";
		public static var ASPECT_RATIO:String="4:3";
		static public var LEVEL_ATLAS:String='level1_sp';  
		static public var citrusRef:CitrusEngine;
		
		static public var DARK_COLOR:uint= 0x168D63; //  0x168D63
		static public var LIGHT_COLOR:uint= 0xF6E89D; //  0xF6E89D
		
		static public var ENEMY_COLOR:uint= 0xFF0000; 
		
		
		public static function get isIOS():Boolean {
			return (Capabilities.manufacturer.indexOf("iOS") != -1);
		}
		
		public static function get isAndroid():Boolean {
			return (Capabilities.manufacturer.toLowerCase().indexOf("android") != -1);
		}
		
		
	}

}