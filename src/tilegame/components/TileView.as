package tilegame.components
{
	import citrus.core.State;
	import citrus.objects.CitrusSprite;
	import citrus.objects.CitrusSpritePool;
	
	import flash.geom.Point;
	
	import tilegame.star.Assets;
	import tilegame.star.ObjectPool;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	
	public class TileView
	{
		private var state:GameState;
		public var upperPoints:Array;
		public var lowerPoints:Array;
		public var upHillBounds,downHillBounds,enemyTimer,enemyInt:Number;
		
		//for hero wall movement
		public var upYLimit:Number;
		public var downYLimit:Number;
		
		private var upHillArr:Array = [1,2,1,1,2,2,2,2,1,2,2,2,1,1,2,1,1,2,1,1];
		private var downHillArr:Array = [1,2,2,2,2,1,1,2,2,2,2,2,1,1,1,2,1,1,2,1,1];
		private var upCount,downCount:Number;
		
		
		
		public function TileView(_state:GameState)
		{
			state =_state;
			upCount=downCount=0;
			
			init();
		}
		
		
		private function _handleUpHillACreate(c:*,params:Object):void
		{
			c.view = starHelper.getImage('hill30000',Assets.LEVEL_ATLAS);
			c.name = 'upHill';
		}
		
		private function _handleUpHillBCreate(c:*,params:Object):void
		{
			c.view = starHelper.getImage('hill40000',Assets.LEVEL_ATLAS);
			c.name = 'upHill';
		}
		
		private function _handleDownHillACreate(c:*,params:Object):void
		{
			c.view = starHelper.getImage('hill10000',Assets.LEVEL_ATLAS);
			c.name = 'downHill';
		}
		
		private function _handleDownHillBCreate(c:*,params:Object):void
		{
			c.view = starHelper.getImage('hill20000',Assets.LEVEL_ATLAS);
			c.name = 'downHill';
		}
		
		
		
		
		
		
		
		
		
		
		private function init(e:*=null):void
		{
			upperPoints = new Array();
			lowerPoints = new Array();
			enemyTimer=upHillBounds=downHillBounds=0;
			var hillCount = 6;
			
			for (var i:int = 0; i < hillCount; i++) 
			{
				addUpHill();
				addDownHill();
				
			}
		}
		
		
		public function addUpHill(e:*=null):void
		{
			upCount++;
			if(upCount == upHillArr.length)upCount=0;
			
			var type:int = upHillArr[upCount];
			switch(type)
			{
				case 1:
				{
					var hillA:CitrusSprite = new CitrusSprite('upHill',{group:1,view:starHelper.getImage('hill30000',Assets.LEVEL_ATLAS)});
					//var hillA = upHillPoolA.get({}).data as CitrusSprite;
					starHelper.setPos(hillA,upHillBounds,-40+40);
					state.add(hillA);
					hillA.touchable=false;
					hillA.updateCallEnabled=false;
					upHillBounds += (hillA.view.width - 2);
					
					state.wallHash.addItem(hillA,1);
					upperPoints.push(new Point(hillA.x + 250,hillA.y + hillA.view.height/2 -30));
					upperPoints.push(new Point(hillA.x + hillA.view.width/2,hillA.y + hillA.view.height -20 - 30));
					upperPoints.push(new Point(hillA.x + hillA.view.width - 290,hillA.y + hillA.view.height/2 - 30));
					
					
					
					break;
				}
					
				case 2:
				{
					
					var hillB = new CitrusSprite('upHill',{group:2,view:starHelper.getImage('hill40000',Assets.LEVEL_ATLAS)});
					//var hillB = upHillPoolB.get({}).data as CitrusSprite;
					
					starHelper.setPos(hillB,upHillBounds,-43+40);
					hillB.group=1;
					hillB.touchable=false;
					hillB.updateCallEnabled=false;
					state.add(hillB);
					upHillBounds += (hillB.view.width - 2);
					
					state.wallHash.addItem(hillB,1);
					upperPoints.push(new Point(hillB.x + 60,hillB.y + hillB.view.height-20 -30));
					upperPoints.push(new Point(hillB.x + hillB.view.width/2,hillB.y + hillB.view.height-20 -30));
					upperPoints.push(new Point(hillB.x + hillB.view.width - 60,hillB.y + hillB.view.height -20 -30));
					
					
					break;
				}
			}
			
			
		}
		
		public function addDownHill(e:*=null):void
		{
			downCount++;
			if(downCount == downHillArr.length)downCount=0;
			
			var type:int = downHillArr[downCount];
			switch(type)
			{
				case 1:
				{
					var hillA:CitrusSprite = new CitrusSprite('downHill',{group:2,view:starHelper.getImage('hill10000',Assets.LEVEL_ATLAS)});
					//var hillA = downHillPoolA.get({}).data as CitrusSprite;
					
					starHelper.setPos(hillA,downHillBounds,505 -90);
					hillA.group=1;
					state.add(hillA);
					downHillBounds += (hillA.view.width-1.4);
					
					state.wallHash.addItem(hillA,1);
					lowerPoints.push(new Point(hillA.x + 250,hillA.y + hillA.view.height/2 -10));
					lowerPoints.push(new Point(hillA.x + hillA.view.width/2,hillA.y -10));
					lowerPoints.push(new Point(hillA.x + hillA.view.width - 290,hillA.y + hillA.view.height/2 - 10 -10));
					
					
					
					break;
				}
					
				case 2:
				{
					
					var hillB = new CitrusSprite('downHill',{group:2,view:starHelper.getImage('hill20000',Assets.LEVEL_ATLAS)});
					//var hillB = downHillPoolB.get({}).data as CitrusSprite;
					
					starHelper.setPos(hillB,downHillBounds,692 -90);
					hillB.group=1;
					state.add(hillB);
					downHillBounds += (hillB.view.width-1.4);
					
					state.wallHash.addItem(hillB,1);
					lowerPoints.push(new Point(hillB.x + 60,hillB.y + 20 -10));
					lowerPoints.push(new Point(hillB.x + hillB.view.width/2,hillB.y + 20 -10));
					lowerPoints.push(new Point(hillB.x + hillB.view.width - 60,hillB.y + 20 -10));
					
					break;
				}
			}
			
			
		}
		
		public function update(heroX:Number):void
		{
			if(upperPoints && lowerPoints)
			{
				var upPt:Point;
				for (var i:int = 0 ; i < upperPoints.length ; i++)
				{
					if (upperPoints.length < 2)
						break;
					
					if (upperPoints[0].x < heroX)
					{
						upPt = upperPoints.shift();					
					}
					else
					{
						break;
					}
				}
				if (upPt) upperPoints.unshift(upPt);
				
				var dwnPt:Point;
				
				for (var j:int = 0 ; j < lowerPoints.length ; j++)
				{
					if (lowerPoints.length < 2)
						break;
					
					if (lowerPoints[0].x < heroX)
					{
						dwnPt = lowerPoints.shift();
					}
					else
					{
						break;
					}
				}
				
				if (dwnPt) lowerPoints.unshift(dwnPt);
				
				
				var lu0Pt:Point = upperPoints[0]; // Upper point 1
				var lu1Pt:Point = upperPoints[1]; // Upper point 2
				var ld0Pt:Point = lowerPoints[0]; // Down point 1
				var ld1Pt:Point = lowerPoints[1]; // Down Point 2
				
				var m1:Number = (lu1Pt.y - lu0Pt.y) / (lu1Pt.x - lu0Pt.x); // Slope 1
				var m2:Number = (ld1Pt.y - ld0Pt.y) / (ld1Pt.x - ld0Pt.x); // Slope 2
				
				// Using y - y1 = m (x - x1), we have x which is hero's x
				upYLimit = (m1 * (heroX - lu0Pt.x)) + lu0Pt.y;
				downYLimit = (m2 * (heroX - ld0Pt.x)) + ld0Pt.y;	
			}
		}
		
		
		
		
	}
}