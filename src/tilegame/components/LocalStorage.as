﻿package tilegame.components 
{
	import com.developmentarc.core.datastructures.utils.HashTable;
	
	import flash.net.SharedObject;
	import flash.utils.ByteArray;

	/**
	 * ...
	 * @author ...
	 */
	public class LocalStorage 
	{
		//////////LOCAL STORAGE FILES NAMES////////////////////////
		public static const PLAYER_DATA:String = "PLAYER_DATA";
		public static var PLAYER_KEY:String = "EASY";
		public static var SETTING_KEY:String = "SETTING_KEY";
		public static var ATTEMPT_KEY:String = "ATTEMPT_KEY";
		
		public function LocalStorage() 
		{
			
		}
		
		
		
		public static function addObject(datakey:String,objectKey:*,objectData:*,todo:String="set"):void
		{
			//todo - add - to add into array, 
			//tod - set - to replace objectdata completely
			
			var so:SharedObject = SharedObject.getLocal(datakey);
			if(todo == "add")
			{
				
				//Add into array
				if (so.data[objectKey])
				{
					var q:Array = (so.data[objectKey] as Array);
					q.push(objectData);
				}
				else
				{
					var queue:Array = new Array();
					queue.push(objectData);
					so.data[objectKey]=queue;
				}
			}
			else if (todo == "set")
			{
				//overrite object data
				so.data[objectKey]= objectData;
			}
			else
			{
				//overrite object data
				so.data[objectKey]= objectData;
			}
			
			so.flush();	//WRITES TO LOCAL FILE//
			
		}
		
		
		public static function getCustomObject(obj:Object,objectClass:Class):*
		{
			trace("LocalStorage.getCustomObject(obj, objectClass)");
			var newObj:* = new objectClass();
			for(var key:* in obj)
			{
				newObj[key]= obj[key];
			}
			
			return newObj;
		}
		
		public static function getObject(datakey:String,objectKey:*):*
		{
			
			var so:SharedObject = SharedObject.getLocal(datakey);
			return so.data[objectKey];
				
		}
		
		public static function getObjectClone(datakey:String,objectKey:*):*
		{
			var so:SharedObject = SharedObject.getLocal(datakey);
			return clone(so.data[objectKey]);
		}
		
		public static function clone( source:Object ):* 
		{ 
			var myBA:ByteArray = new ByteArray(); 
			myBA.writeObject( source ); 
			myBA.position = 0; 
			return( myBA.readObject() ); 
		}

		public static function destroySharedObject(datakey:String):void 
		{ 
			var so:SharedObject = SharedObject.getLocal(datakey);
			so.clear(); 
		}
		
		public static function reset():void 
		{ 
			destroySharedObject(PLAYER_DATA);
		
		}
		
	}

}