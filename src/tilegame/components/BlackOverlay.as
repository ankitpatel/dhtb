﻿package tilegame.components
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.TweenNano;
	import com.greensock.easing.Strong;
	
	import feathers.display.Scale9Image;
	
	import flash.geom.Rectangle;
	
	import tilegame.star.Assets;
	import tilegame.star.starHelper;
	
	import starling.display.Quad;
	import starling.display.Sprite;

	public class BlackOverlay extends Sprite
	{
		
		private var bg:Quad;
		public function BlackOverlay(width:*=100,height:*=100,alpha:*=1,twn:Boolean=false)
		{
			bg = new Quad(width,height,0x000000);
			bg.touchable=true;
			this.addChild(bg);
			
			this.width = width;
			this.height= height;
			
			if(!twn){
				bg.alpha = alpha;
			}
			else
			{
				bg.alpha = 0;
				TweenNano.to(bg,0.5,{ alpha:alpha});
					
			}
			
		}
		
		public function reverseTwn(callback:Function=null):void
		{
			TweenNano.to(bg,0.5,{ alpha:0,onComplete:callback});
			
		}
	}
}