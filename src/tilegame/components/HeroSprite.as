﻿package tilegame.components
{
	import citrus.core.CitrusEngine;
	import citrus.input.controllers.Keyboard;
	import citrus.objects.CitrusSprite;
	import citrus.objects.vehicle.nape.Nugget;
	import citrus.view.starlingview.StarlingArt;
	
	import com.gestouch.gestures.SwipeGestureDirection;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import org.osflash.signals.Signal;
	
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	
	import tilegame.CitrusGame;
	import tilegame.Constants;
	import tilegame.sound.MusicHelper;
	import tilegame.sound.SoundHelper;
	import tilegame.star.GestureHelper;
	import tilegame.star.StarEventHelper;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;
	
	public class HeroSprite extends CitrusSprite
	{
		private var _mobileInput:MobileInput;
		public var boost:Number=0;
		public var magnet:Boolean=false;
		public var shield:Boolean=false;
		public var reset:Boolean=false;
		public var hit:Boolean=false;
		public var enableGravity:Boolean=false;
		public var enableBubbles:Boolean=false;
		private var prevX:Number=0;
		private var wall:Boolean=false;
		public var onPause:Signal;
		private var kb:Keyboard;
		private var firstTime:Boolean=true;
		private var xOffset:Number=0;
		public var baseSpeedX:Number=20;
		public var baseSpeedY:Number=0;
		private var isTouched:Boolean=false;
		private var boostEnd,shieldEnd,magnetEnd:Boolean=false;
		private var magnetTime,shieldTime:Number;
		public var lastPosX:Number;
		private var prevDis:Number=0;
		private var effectTime:Number=6;
		private var magnetUnit:Number;
		private var shieldUnit:Number;
		public static var heroAnimArr:Array = ['hit','idle'];
		
		private var gestureHelper:GestureHelper;
		public var curTrack:Number;
		
		
		private var velocityY:Number;
		private var velocityOffset:Number;
		
		
		public function HeroSprite(name:String, params:Object = null)
		{
			super(name, params);
			this.updateCallEnabled=true;
			if(LocalStorage.PLAYER_KEY == Constants.EASY)
			{
				velocityY = -400;
				velocityOffset = -0.25;
			}
			else if(LocalStorage.PLAYER_KEY == Constants.MEDIUM)
			{
				velocityY = -400;
				velocityOffset = -0.3;
			}
			else if(LocalStorage.PLAYER_KEY == Constants.HARD)
			{
				velocityY = -400;
				velocityOffset = -0.4;
			}
			
			
			_mobileInput = new MobileInput();
			_mobileInput.initialize();
			onPause= new Signal();
			reset=false;
			hit=false;
			firstTime=true;
			curTrack = 1;
			//StarlingArt.setLoopAnimations(heroAnimArr);
			
			kb = _ce.input.keyboard;
			kb.addKeyAction("left", Keyboard.LEFT);
			kb.addKeyAction("right", Keyboard.RIGHT);
			
		}
		
		public function addTouchEvents(e:*=null):void
		{
			
			gestureHelper = new GestureHelper();
			//gestureHelper.addTapGesture(starHelper.stageRef,1);
			//gestureHelper.addEventListener(GestureHelper.SINGLE_TAP,onTap);
			
			gestureHelper.addSwipeGesture(starHelper.stageRef,SwipeGestureDirection.RIGHT,30);
			gestureHelper.addSwipeGesture(starHelper.stageRef,SwipeGestureDirection.LEFT,30);
			gestureHelper.addEventListener(GestureHelper.SWIPE_LEFT,onSwipeLeft);
			gestureHelper.addEventListener(GestureHelper.SWIPE_RIGHT,onSwipeRight);
			
			
		}
		
		
		private function onSwipeLeft(e:*=null):void
		{
			if(curTrack > 0 && !GameState.gameEnd)
			{
				(_ce.state as GameState).scoreBar.update(1);
				//left
				SoundHelper.play(SoundHelper.SLIDE,1,1,true);
				
				curTrack --;
				TweenLite.to(this.view,0.1,{x:"-"+ObjectView.TILE_WIDTH});
			}
		}
		
		private function onSwipeRight(e:*=null):void
		{
			if(curTrack < (ObjectView.TOTAL_TRACKS -1) && !GameState.gameEnd)
			{
				(_ce.state as GameState).scoreBar.update(1);
				
				//right
				SoundHelper.play(SoundHelper.SLIDE,1,1,true);
				
				curTrack++;
				TweenLite.to(this.view,0.1,{x:"+"+ObjectView.TILE_WIDTH});
			}
			
		}
		
		private function onTap(e:*=null):void
		{
			trace("HeroSprite.TAP(e)",this.view.x);
			if(_ce.input.hasDone('left')){
				onSwipeLeft();
			}
			else
			{
				onSwipeRight();
			}
			
		}
		
		override public function destroy():void {
			
			_mobileInput.destroy();
			gestureHelper.removeEventListener(GestureHelper.SINGLE_TAP,onTap);
			gestureHelper.dispose();
			
			super.destroy();
		}
		
		override public function update(timeDelta:Number):void {
			
			super.update(timeDelta);
			
			if(hit)
			{
				_velocity.x = 0;
				_velocity.y=0;
				
			}
			else 
			{
				
				velocityY += velocityOffset;
				_velocity.y = velocityY;
				
				if(_ce.input.hasDone('left') || _ce.input.hasDone('right'))
				{
					onTap();
				}
				
			}
			
			
			//_updateAnimation();
			
		}
		
		
		public function onEnemyHit(collider:*=null):void
		{
			trace("FlyingHeroSprite.onEnemyHit(collider, heroArt)");
			if(!hit)
			{
				//trace("hit name>", collider.name);
				//MusicHelper.pause(MusicHelper.MENU);
				SoundHelper.play(SoundHelper.HEROHIT,1,1,true);
				
				hit= true;
				CitrusGame._camera.enabled=false;
				
				collider.view.color = Constants.ENEMY_COLOR;
				collider.view.alpha=0.5;
				TweenMax.to(collider.view,0.1,{alpha:0.25,repeat:3,yoyo:true,onComplete:onCollisionAnimOver});
			}
			
			
		}
		
		private function disposeEnemy(ene:*):void
		{
			ene.kill=true;
		}
		
		private function onCollisionAnimOver(e:*=null):void
		{
			onPause.dispatch();
		}
		
		
		private function _updateAnimation():void {
			
			if(hit)
			{
				_animation ='hit';
			}
			else
			{
				_animation = "idle";
				
			}
			
			
		}
		
	}
}