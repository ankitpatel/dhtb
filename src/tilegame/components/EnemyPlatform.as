﻿package tilegame.components
{
	import Box2D.Dynamics.Contacts.b2Contact;
	
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.box2d.Platform;
	import citrus.objects.platformer.box2d.Sensor;
	import citrus.physics.box2d.Box2DUtils;
	import citrus.physics.box2d.IBox2DPhysicsObject;
	import citrus.view.starlingview.StarlingArt;
	
	import tilegame.states.GameState;
	
	import starling.events.TouchEvent;

	public class EnemyPlatform extends CitrusSprite
	{
		public var enemyType:String;
		
		public function EnemyPlatform(name:String, params:Object = null)
		{
			super(name, params);
			
		}
		
		override public function update(timeDelta:Number):void {
			super.update(timeDelta);
			
			
		}
		
	}
}