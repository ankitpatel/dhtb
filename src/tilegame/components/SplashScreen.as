﻿package tilegame.components {
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import tilegame.Constants;
	import tilegame.star.*;
	
	
	public class SplashScreen extends Sprite {
		
		public static const NAME:String = "SplashScreen";
		public static const ON_PLAY:String = "ON_PLAY";
		
		private var playBtn:Image;
		
		public function SplashScreen() {
			// constructor code
			
			var titleTxt:TextField = starHelper.addTextField(this,"Make Ball Fall",50,Constants.DEVICE_WIDTH/2,100,Constants.LIGHT_COLOR,600,120,false,true);
			
			var playBtn:FlatButton = new FlatButton(this,'Play',200,50,onClickPlayBtn);
			starHelper.setPos(playBtn,Constants.DEVICE_WIDTH/2,Constants.DEVICE_HEIGHT/2 - 50);
			
			var settingBtn:FlatButton = new FlatButton(this,'Settings',200,50);
			starHelper.setPos(settingBtn,Constants.DEVICE_WIDTH/2,Constants.DEVICE_HEIGHT/2+ 50);
			
			
		}
		
		
		private function onClickPlayBtn(e:*=null):void 
		{
			dispatchEventWith(ON_PLAY);
			
		}
		
		public function remove(e:*=null):void
		{
			starHelper.removeAll(this);
			
		}
		
		
	}
	
}
