﻿package tilegame.components
{
	import com.developmentarc.core.datastructures.utils.HashTable;
	
	import flash.events.TimerEvent;
	import flash.sampler.NewObjectSample;
	import flash.utils.Timer;

	/**
	 * ...
	 * @author ...
	 */
	public class TimerHelper extends Timer
	{
		static public var arrTimers:HashTable= new HashTable();
		
		public function TimerHelper(time:Number,repeatCount:int=0,handler:Function=null) 
		{
			super(time, repeatCount);
			if(handler != null)this.addEventListener(TimerEvent.TIMER, handler);
			arrTimers.addItem(this,handler);
			
		}
		
		
		public static function pauseAllTimers(remove:Boolean=false):void
		{
			
		   for (var i:Number = 0; arrTimers && i < arrTimers.length; i++)
		   {
			   var timer:*= arrTimers.getKeyAt(i);
				if (timer)
				{
					timer.stop();	
					if(remove){
						timer.removeEventListener(TimerEvent.TIMER, arrTimers.getItem(timer));
						arrTimers.remove(timer);
						timer=null;
					}
				}
		   }
		   
		}
		
		public static function resumeAllTimers():void
		{
			
			for (var i:Number = 0; arrTimers && i < arrTimers.length; i++)
			{
				var timer:*= arrTimers.getKeyAt(i);
				if (timer)
				{
					timer.start();	
				}
			}
		   
		}
		
		
	}

}