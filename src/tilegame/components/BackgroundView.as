﻿package tilegame.components
{
	import citrus.core.State;
	import citrus.objects.CitrusSprite;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	
	import tilegame.Constants;
	import tilegame.star.Assets;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;
	
	public class BackgroundView extends Sprite
	{
		private var state:*;
		protected var sandBG1:CitrusSprite;
		protected var sandBG2:CitrusSprite;
		protected var upperPoints:Array;
		protected var lowerPoints:Array;
		protected var upHillBounds,downHillBounds,enemyTimer,enemyInt:Number;
		
		public function BackgroundView(_state:*)
		{
			state =_state;
			this.addEventListener(Event.ADDED_TO_STAGE,init);
			
		}
		
		private function init(e:*=null):void
		{
			/*
			var viewroot:Sprite = state.getChildAt(0) as Sprite;
			var bg:Image = starHelper.getImage('gameBG0000',Assets.LEVEL_ATLAS);
			this.addChild(bg);
			state.swapChildren(this,viewroot);
			*/
			
			if(!sandBG1)
			{
				var img1:Image =starHelper.getImage('bushBG0000',Assets.LEVEL_ATLAS);
				img1.scaleY = -1;
				sandBG1 = new CitrusSprite("bushBG", {group:0, view:img1})
				starHelper.setPos(sandBG1,0,Constants.DEVICE_HEIGHT);
				trace('state',state,sandBG1);
				state.add(sandBG1);
				sandBG1.touchable=false;
				sandBG1.updateCallEnabled=false;
			}
			
			if(!sandBG2)
			{
				var img2:Image =starHelper.getImage('bushBG0000',Assets.LEVEL_ATLAS);
				img2.scaleY = -1;
				sandBG2 = new CitrusSprite("bushBG", {group:0, view:img2})
				starHelper.setPos(sandBG2,0,0);
				state.add(sandBG2);
				sandBG2.touchable=false;
				sandBG2.updateCallEnabled=false;
			}
			
		}
		
		public function update(heroY:Number)
		{
			if(sandBG1 && sandBG2)
			{
				trace("heroY,sandBG1,diff",heroY,sandBG1,Math.abs(heroY - sandBG1.y));
				if(heroY - sandBG1.y < 0 && Math.abs(heroY - sandBG1.y) > sandBG1.view.height)
				//if(Math.abs((Math.abs(heroY) - Math.abs(sandBG1.y))) > 1.2*Constants.DEVICE_HEIGHT)
				{
					sandBG1.y = sandBG1.y - (2*sandBG1.view.height);
				}
				else if(heroY - sandBG1.y < 0 && Math.abs(heroY - sandBG2.y) > sandBG2.view.height)
				//if(Math.abs((Math.abs(heroY) - Math.abs(sandBG2.y))) > 1.2*Constants.DEVICE_HEIGHT)
				{
					sandBG2.y = sandBG2.y - (2*sandBG2.view.height);
				}
				
			}
			
		}
		
		public function remove(e:*=null):void
		{
			sandBG1.kill = true;
			sandBG2.kill =true;
			upperPoints=null;
			lowerPoints=null;
		}
	}
}