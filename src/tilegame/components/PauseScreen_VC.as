﻿package tilegame.components 
{
	import citrus.core.CitrusEngine;
	
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import com.greensock.TweenNano;
	
	import tilegame.Constants;
	import tilegame.star.Assets;
	import tilegame.star.StarEventHelper;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	/**
	 * ...
	 * @author ...
	 */
	public class PauseScreen_VC extends Sprite
	{
		
		public var replay_btn:*;
		public var menu_btn:*;
		public var micro_btn:*;
		public var cont_btn:*;
		public var buyPlayBtn,buyBoostBtn,buyMenuBtn:*;
		private var popup:Sprite;
		
		static public const ON_START:String='ON_START';  
		static public const ON_PAUSE:String='ON_PAUSE';  
		static public const ON_END:String='ON_END'; 
		
		static public const ON_MENU:String='ON_MENU';  
		static public const ON_REPLAY:String='ON_REPLAY';  
		
		private var player1:PlayerIcon;
		private var player2:PlayerIcon;
		private var curPlayer:PlayerIcon;
		private var infoTxt:TextField;
		private var scoreBoostTxt:TextField;
		private var infoPosX,infoPosY:Number;
		private var type:String;
		
		public function PauseScreen_VC(_type:String) 
		{
			trace("PauseScreen_VC.PauseScreen_VC(type)",Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT);
			type = _type;
			var bg =new BlackOverlay(1024,768,0.8,false);
			this.addChild(bg);
			
			popup = new Sprite();
			this.addChild(popup);
			starHelper.setPos(popup,0,0,[70,-25]);
			
			switch(type)
			{
				case ON_PAUSE:
				{
					cont_btn= starHelper.addObject(popup,starHelper.IMG,"platform_cont_btn",Constants.LEVEL_ATLAS,512,454,30,true,true);
					replay_btn= starHelper.addObject(popup,starHelper.IMG,"platform_replay_btn",Constants.LEVEL_ATLAS,211,320,30,true,true);
					menu_btn= starHelper.addObject(popup,starHelper.IMG,"platform_menu_btn",Constants.LEVEL_ATLAS,803,320,30,true,true);
					cont_btn.alpha=0.1;
					replay_btn.alpha=0.3;
					menu_btn.alpha=0.1;
					cont_btn.scaleX = 3;
					cont_btn.scaleY = 3;
					
					var timeLine = new TimelineLite({onComplete:addPauseListeners});
					timeLine.appendMultiple([new TweenLite(replay_btn,0.3,{x:starHelper.getXPos(400)}),new TweenLite(menu_btn,0.3,{x:starHelper.getXPos(610),alpha:1}),new TweenLite(cont_btn,0.3,{scaleX:1,scaleY:1,alpha:1})]);
					
					
					break;
				}
					
				case ON_END:
				{
					bg.visible=false;
					
					var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY);
					starHelper.addTextField(popup,'Your Score',55,512,170,0xffffff,500,80,false,true,"museo");
					starHelper.addTextField(popup,ScoreBar.distance.toString(),65,512,290,0xFFFF00,500,95,false,true,"museo");
					//starHelper.addTextField(popup,'Best Score - ' + localStorageObj.bestscore,30,512,330,0xffffff,500,85,false,true,"museo");
					
					buyPlayBtn = starHelper.addObject(popup,starHelper.IMG,"scorecard_play",Constants.LEVEL_ATLAS,412-150,495,30,true,true);
					buyBoostBtn = starHelper.addObject(popup,starHelper.IMG,"scorecard_boost",Constants.LEVEL_ATLAS,512,495,30,true,true);
					buyMenuBtn = starHelper.addObject(popup,starHelper.IMG,"scorecard_menu",Constants.LEVEL_ATLAS,612+150,495,30,true,true);
					
					infoTxt= starHelper.addTextField(popup,"Not enough coins!",20,515,400,0xcc3300,400,40);
					infoTxt.visible=false;
					
					//boost tag
					var scorecard_boostlabel = starHelper.addObject(popup,starHelper.IMG,"scorecard_boostlabel",Constants.LEVEL_ATLAS,900,50,30,true,true);
					scoreBoostTxt= starHelper.addTextField(popup,localStorageObj.boost,75,scorecard_boostlabel.x + 15,scorecard_boostlabel.y + 5,0xffffff,200,55,false,true,"museo");
					
					
					StarEventHelper.addTouchEventListener(buyPlayBtn,StarEventHelper.BUTTON, onReplay);
					StarEventHelper.addTouchEventListener(buyMenuBtn,StarEventHelper.BUTTON, onMenu);
					
					TweenNano.from(popup,0.5,{y:"-800"});
					
					break;
				}
			}
			
			
			if(Constants.SCREEN_WIDTH == 1136)
			{
				popup.scaleX = 0.9;
				popup.x += 40;
			}
			
			
		}
		
		public function addPauseListeners(e:*=null):void
		{
			StarEventHelper.addTouchEventListener(menu_btn,StarEventHelper.BUTTON, onMenu);
			StarEventHelper.addTouchEventListener(cont_btn,StarEventHelper.BUTTON, onReplay);
		}
		
		private function onMenu(e:*=null):void
		{
			dispatchEventWith(ON_MENU);
			
		}
		
		private function onReplay(e:*=null):void
		{
			dispatchEventWith(ON_REPLAY);
		}
		
		
		
		public function remove(e:*=null):void
		{
			
			starHelper.removeAll(this);
			if(replay_btn){
				replay_btn.removeEventListeners();
				replay_btn=null;
			}
			if(menu_btn){
				menu_btn.removeEventListeners();
				menu_btn=null;
				
			}
			if(micro_btn){
				micro_btn.removeEventListeners();
				micro_btn=null;
				
			}
			if(cont_btn){
				cont_btn.removeEventListeners();
				cont_btn=null;
				
			}
			
		}
	}
	
}