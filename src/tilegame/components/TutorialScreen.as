﻿package tilegame.components
{
	import citrus.core.CitrusEngine;
	
	import com.greensock.TimelineMax;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import tilegame.Constants;
	import tilegame.star.StarEventHelper;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;

	public class TutorialScreen extends Sprite
	{
		private var popup:Sprite;
		static public const ON_NEXT:String='ON_NEXT';  

		private var leftBG:BlackOverlay;
		private var rightBG:BlackOverlay;

		private var nextBtn:*;

		private var twnEffect:*;
		
		
		public function TutorialScreen()
		{
			leftBG =new BlackOverlay(Constants.DEVICE_WIDTH,Constants.DEVICE_HEIGHT,0.65,true);
			this.addChild(leftBG);
			starHelper.setPos(leftBG,0,0);
			
			popup = new Sprite();
			this.addChild(popup);
			
			var leftArrow= starHelper.addObject(popup,starHelper.IMG,"arrow",Constants.LEVEL_ATLAS,140,500,30,true,true);
			leftArrow.scaleX=-1;
			
			var infoMessage:String = "SWIPE TO THE DIRECTION YOU WANT TO MOVE";
			if(CONFIG::AIR){
				infoMessage ="SWIPE TO THE DIRECTION YOU WANT TO MOVE";
			}
			else
			{
				infoMessage ="USE LEFT-RIGHT ARROW KEYS TO MOVE";
				
			}
			var leftInfoTxt= starHelper.addTextField(popup,infoMessage,27,360,500,0xffffff,350,150,false,true,"museo");
			var rightArrow= starHelper.addObject(popup,starHelper.IMG,"arrow",Constants.LEVEL_ATLAS,580,500,30,true,true);
			
			nextBtn = new FlatButton(this,"NEXT",200,60,onNextClick);
			starHelper.setPos(nextBtn,370,650);
			//nextBtn.visible=false;
			
			twnEffect = new TimelineMax({onComplete:enableNextBtn});
			twnEffect.insertMultiple( 
				[	
					TweenLite.from([leftInfoTxt,leftArrow,rightArrow], 0.5,{alpha:0,x:"-=600",ease:Back.easeOut}),
					TweenLite.from([nextBtn], 0.5,{alpha:0,x:"+=600",ease:Back.easeOut})
					
				],0.1,"normal",0.2);
			
		}
		
		private function enableNextBtn(e:*=null):void
		{
			nextBtn.visible=true;
			TweenMax.to(nextBtn,0.3,{alpha:0.9,repeat:-1,yoyo:true});
		}
		
		public function endTweenEffect(callback:Function):void
		{
			leftBG.reverseTwn();
			twnEffect.reverse();
			TweenLite.delayedCall(0.6,callback);
		}
		
		private function onNextClick(e:*=null):void
		{
			endTweenEffect(dispatchNextEvent);
		}
		
		private function dispatchNextEvent(e:*=null):void
		{
			dispatchEventWith(ON_NEXT);
		}
		
	}
}