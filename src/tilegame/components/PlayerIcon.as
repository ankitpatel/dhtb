﻿package tilegame.components
{
	import dragonBones.objects.Frame;
	
	import tilegame.Constants;
	import tilegame.star.starHelper;
	
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.HAlign;

	public class PlayerIcon extends Sprite
	{
		private var _coins:Number;
		private var _boost:Number;
		public var playerid:String;

		private var coinTxt,boostTxt,nameTxt:TextField;
		public var vo:Object;
		private var icon:MovieClip;
		
		public function PlayerIcon(_playerVO:*,frame:int=0)
		{
			vo = _playerVO;
			icon= starHelper.addObject(this,starHelper.MOVIECLIP,"playerIcon",Constants.LEVEL_ATLAS,0,0,30,true,true);
			boostTxt= starHelper.addTextField(this,'',35,-60,-34,0xffffff,90,35,false,true,"museo");
			boostTxt.hAlign = HAlign.LEFT;
			coinTxt= starHelper.addTextField(this,'',80,-60,10,0xffffff,90,30,false,true,"museo");
			(coinTxt as TextField).hAlign = HAlign.LEFT;
			nameTxt= starHelper.addTextField(this,vo.playername,35,75,60,0xffffff,100,35,false,true,"museo");
			
			coins = vo.coins;
			boost = vo.boost;
			playerid = vo.playerid;
			
			trace("PlayerIcon.PlayerIcon(_playerVO, frame)",playerid,coins);
			
			setFrame(frame);
		}
		
		public function setFrame(frame:int=0):void
		{
			starHelper.gotoAndStop(icon,frame);
		}

		public function get boost():Number
		{
			return _boost;
		}

		public function set boost(value:Number):void
		{
			_boost = value;
			boostTxt.text = _boost.toString();
		}

		public function get coins():Number
		{
			return _coins;
		}

		public function set coins(value:Number):void
		{
			_coins = value;
			coinTxt.text = _coins.toString();
			
		}

	}
}