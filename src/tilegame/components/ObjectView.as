﻿package tilegame.components
{
	import citrus.math.MathVector;
	import citrus.objects.Box2DObjectPool;
	import citrus.objects.CitrusSprite;
	import citrus.objects.CitrusSpritePool;
	import citrus.objects.platformer.box2d.Coin;
	import citrus.objects.vehicle.nape.Nugget;
	import citrus.physics.simple.SimpleCitrusSolver;
	
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	
	import tilegame.Constants;
	import tilegame.star.Assets;
	import tilegame.star.starHelper;
	import tilegame.states.GameState;
	
	public class ObjectView
	{
		private var state:GameState;
		protected var enemyDelay;
		private var prevType:String;
		private var prevRect:Rectangle;
		private var enemyTimer:TimerHelper;
		private var fishTimer:TimerHelper;
		private var coinTimer:TimerHelper;
		private var enemeyGap:Number = 600;
		private var startHeroX:Number=0;
		public var enemyYOffset:Number;
		public var posXArr:Array;
		public var lastPosIndex:int;
		
		private var firstTile:Boolean;
		private var firstPos:Number;
		
		//STATIC VARS
		static public var TILE:String='Tile'; 
		static public var TILE_WIDTH:Number;
		static public var TOTAL_TRACKS:int = 4;
		
		
		public function ObjectView(_state:GameState)
		{
			state =_state;
			enemyDelay = 400;
			lastPosIndex = 1;
			
			if(LocalStorage.PLAYER_KEY == Constants.EASY)
			{
				firstTile =true;
				firstPos = Constants.DEVICE_HEIGHT;
				TOTAL_TRACKS =6;
				enemyYOffset =Constants.DEVICE_HEIGHT;
			}
			else if(LocalStorage.PLAYER_KEY == Constants.MEDIUM)
			{
				firstTile =true;
				firstPos = Constants.DEVICE_HEIGHT;
				TOTAL_TRACKS = 7;
				enemyYOffset =Constants.DEVICE_HEIGHT;
				
			}
			else if(LocalStorage.PLAYER_KEY == Constants.HARD)
			{
				firstTile =true;
				firstPos = Constants.DEVICE_HEIGHT;
				TOTAL_TRACKS = 9;
				enemyYOffset =Constants.DEVICE_HEIGHT;
				
			}
			
			
			posXArr = new Array();
			var startPosX=0;
			for (var i:int = 0; i < TOTAL_TRACKS; i++) 
			{
				posXArr.push(startPosX);
				startPosX += Constants.DEVICE_WIDTH/TOTAL_TRACKS;
			}
			TILE_WIDTH = Constants.DEVICE_WIDTH/posXArr.length;
			
			enemyTimer = new TimerHelper(enemyDelay,0,addEnemy);
			enemyTimer.start();
			
			for (var j:int = 0; j < 5; j++) 
			{
				addEnemy();
			}
			
			
		}
		
		
		protected function addEnemy(e:*=null):void
		{
			trace('enemy',state.wallHash.length);
			if(state.wallHash.length > 60)return;
			var enemyArr:Array  = [TILE];
			var enemyType:String;
			enemyType = TILE;
			setEnemyPosition(enemyType);
		}
		
		private function setEnemyPosition(enemyType:String):void
		{
			var enemyPosX,enemyPosY:Number;
			var enemy:EnemyPlatform;
			var enemyView:Sprite = new Sprite();;
			
			var tileWidth:Number = TILE_WIDTH;
			var tileHeight:Number = tileWidth*1.5;
			var tileLen:int;
			if(firstTile){
				tileLen = 10;
			}
			else
			{
				if(LocalStorage.PLAYER_KEY == Constants.EASY)
				{
					tileLen =5 + int(Math.random()*3);
				}
				else if(LocalStorage.PLAYER_KEY == Constants.MEDIUM)
				{
					tileLen =5 + int(Math.random()*2);
				}
				else if(LocalStorage.PLAYER_KEY == Constants.HARD)
				{
					tileLen =4 + int(Math.random()*2);
				}
			}
			
			
			if(firstTile){
				enemyPosX = posXArr[lastPosIndex];	
				enemyPosY = firstPos;
			}
			else
			{
				var direction:int=0;
				if(lastPosIndex == 0)
				{
					lastPosIndex++;
					direction =1;
				}
				else if(lastPosIndex == (posXArr.length-1))
				{
					lastPosIndex = posXArr.length -2;
					direction =0;
				}
				else
				{
					if((Math.random() > 0.5))
					{
						lastPosIndex--;
						direction =0;
					}
					else
					{
						lastPosIndex++;
						direction =1;
					}
					
				}
				
				enemyPosX = posXArr[lastPosIndex];
				enemyPosY = enemyYOffset;
				
			}
			
			addBush(enemyPosX,enemyPosY);
			
			for (var i:int = 0; i < tileLen; i++) 
			{
				var tile:Quad = new Quad(tileWidth,tileHeight,Constants.LIGHT_COLOR);
				starHelper.setPos(tile,0,enemyView.height);
				enemyView.addChild(tile);
				
				if((firstTile || i!=0) && i!=tileLen-1)
				{
					//trace("???",i,tileLen);
					var pathQuad1:Quad = new Quad(tileWidth,tileHeight,Constants.DARK_COLOR);
					pathQuad1.touchable=false;
					var pathSprite1 = new CitrusSprite('wall',{group:3,view:pathQuad1});
					starHelper.setPos(pathSprite1,enemyPosX - tileWidth,enemyPosY-enemyView.height);
					state.add(pathSprite1);	
					state.wallHash.addItem(pathSprite1,1);
					
					var pathQuad2:Quad = new Quad(tileWidth,tileHeight,Constants.DARK_COLOR);
					pathQuad2.touchable=false;
					var pathSprite2 = new CitrusSprite('wall',{group:3,view:pathQuad2});
					starHelper.setPos(pathSprite2, enemyPosX + tileWidth,enemyPosY-enemyView.height);
					state.add(pathSprite2);	
					state.wallHash.addItem(pathSprite2,1);
				}
				else
				{
					if(i == 0)
					{
						//trace("???",i,tileLen);
						
						var pathQuad1:Quad = new Quad(tileWidth,tileHeight,Constants.DARK_COLOR);
						pathQuad1.touchable=false;
						var pathSprite1 = new CitrusSprite('wall',{group:3,view:pathQuad1});
						starHelper.setPos(pathSprite1,direction ? enemyPosX - 2*tileWidth : enemyPosX - tileWidth,enemyPosY-enemyView.height);
						state.add(pathSprite1);	
						state.wallHash.addItem(pathSprite1,1);
						
						var pathQuad2:Quad = new Quad(tileWidth,tileHeight,Constants.DARK_COLOR);
						pathQuad2.touchable=false;
						var pathSprite2 = new CitrusSprite('wall',{group:3,view:pathQuad2});
						starHelper.setPos(pathSprite2,direction ? enemyPosX + tileWidth: enemyPosX + 2*tileWidth,enemyPosY-enemyView.height);
						state.add(pathSprite2);	
						state.wallHash.addItem(pathSprite2,1);
						
					}
				}
			}
			
			enemyView.scaleY = -1;
			enemyView.name = enemyType;
			enemy= new EnemyPlatform(enemyType,{group:4,view:enemyView});
			enemy.touchable=false;
			state.add(enemy);	
			starHelper.setPos(enemy,enemyPosX, enemyPosY);	
			state.wallHash.addItem(enemy,1);
			enemyYOffset = enemyYOffset - (tileHeight*(tileLen-1)) ;
			firstTile =false;
		}
		
		private function addBush(enemyPosX:Number,enemyPosY:Number):void
		{
			var posX:Number;
			var posY:Number = enemyPosY - (Math.random()*200);
			
			if(Math.random()>0.5)
			{
				posX = enemyPosX + (300+Math.random()*50);
			}
			else
			{
				posX = enemyPosX - (300+Math.random()*50);
			}
			
			for (var i:int = 0; i < 2 + int(Math.random()*3); i++) 
			{
				var bushImg:Image = starHelper.getImage('bush0000',Assets.LEVEL_ATLAS);
				starHelper.setScale(bushImg,1+ Math.random()*0.5);
				//bushImg.rotation = Math.random()* 3.14;
				bushImg.touchable=false;
				var bushSprite = new CitrusSprite('plant',{group:4,view:bushImg});
				starHelper.setPos(bushSprite,posX,posY + i*bushImg.height + 40);
				state.add(bushSprite);	
				state.wallHash.addItem(bushSprite,1);
			}
			
		}
		
		public function activate(e:*=null):void
		{
			enemyTimer.start();
		}
		
		public function deactivate(e:*=null):void
		{
			enemyTimer.stop();
		}
		
		
	}
}