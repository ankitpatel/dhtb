﻿package tilegame.components
{
	import feathers.display.Scale9Image;
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import tilegame.Constants;
	import tilegame.star.Assets;
	import tilegame.star.StarEventHelper;
	import tilegame.star.starHelper;

	public class GameModeButton extends Sprite
	{

		private var clickHandler:Function;
		private var _text1:String;
		private var _text2:String;
		private var txtField1:TextField;
		private var txtField2:TextField;
		
		
		public function GameModeButton(parent:Sprite,_text1:String=null,_text2:String=null,_width:Number=200,_height:Number=60,_clickHandler:Function=null)
		{
			clickHandler = _clickHandler;
			var bg:Scale9Image = starHelper.getScale9Image('buttonBG0000',Assets.LEVEL_ATLAS,new Rectangle(15,15,10,10),_width,_height,true);
			this.addChild(bg);
			if(_text1)txtField1= starHelper.addTextField(this,_text1,35,_width/2,(_text2 != null) ? 35 : _height/2,Constants.DARK_COLOR,_width,_height,true,true);
			if(_text2)txtField2=starHelper.addTextField(this,_text2,25,_width/2,bg.height - 35,Constants.DARK_COLOR,_width,25,true,true);
			
			if(clickHandler)StarEventHelper.addTouchEventListener(this,StarEventHelper.BUTTON,clickHandler);
			
			starHelper.setPivot(this);
			parent.addChild(this);
		}
		
		public function activate(e:*=null):void
		{
			this.touchable= true;
		}
		
		public function deactivate(e:*=null):void
		{
			this.touchable=false;
		}
		
	
		
		public function get text1():String
		{
			if(txtField1){
				return txtField1.text;
			}
			return null;
		}
		
		public function set text1(value:String):void
		{
			if(txtField1)txtField1.text = value;
		}
		
		public function get text2():String
		{
			if(txtField2){
				return txtField2.text;
			}
			return null;
		}
		
		public function set text2(value:String):void
		{
			if(txtField2)txtField2.text = value;
		}

		
		public function remove(e:*=null):void
		{
			if(clickHandler)StarEventHelper.removeTouchEventListener(this,clickHandler);
			this.removeEventListeners();		
		}
	}
}