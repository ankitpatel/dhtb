package tilegame.components
{
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import tilegame.Constants;
	import tilegame.star.Assets;
	import tilegame.star.StarEventHelper;
	import tilegame.star.starHelper;

	public class FlatButton extends Sprite
	{
		private var clickHandler:Function;
		
		public function FlatButton(parent:Sprite,_text:String=null,_width:Number=200,_height:Number=60,_clickHandler:Function=null)
		{
			clickHandler = _clickHandler;
			var bg = starHelper.getScale9Image('buttonBG0000',Assets.LEVEL_ATLAS,new Rectangle(15,15,10,10),_width,_height,true);
			this.addChild(bg);
			if(_text)var btnTxt:TextField = starHelper.addTextField(this,_text,25,_width/2,_height/2,Constants.DARK_COLOR,_width,_height,true,true);
			
			if(clickHandler)StarEventHelper.addTouchEventListener(this,StarEventHelper.BUTTON,clickHandler);
			
			starHelper.setPivot(this);
			parent.addChild(this);
		}
		
		public function activate(e:*=null):void
		{
			this.touchable= true;
		}
		
		public function deactivate(e:*=null):void
		{
			this.touchable=false;
		}
		
		public function remove(e:*=null):void
		{
			if(clickHandler)StarEventHelper.removeTouchEventListener(this,clickHandler);
			this.removeEventListeners();		
		}
	}
}