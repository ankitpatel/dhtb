﻿package tilegame.components
{
	import com.playpower.logger.utils.setTimeout;
	
	import feathers.display.Scale9Image;
	
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.utils.HAlign;
	
	import tilegame.Constants;
	import tilegame.star.Assets;
	import tilegame.star.starHelper;

	public class ScoreBar extends Sprite
	{
		public static var distance:Number=0;
		private var disTxt:TextField;
		public var bestscore:*=null;
		public var isBestScoreEnabled:Boolean;
		
		public function ScoreBar()
		{
			distance=0;
			
			var bg:Scale9Image = starHelper.getScale9Image('buttonBG0000',Assets.LEVEL_ATLAS,new Rectangle(15,15,10,10),200,100,true);
			//bg.alpha=0.5;
			this.addChild(bg);
			
			var scoreTitle = starHelper.addTextField(this,"SCORE",40,bg.width/2,bg.height/2 - 20,Constants.DARK_COLOR,200,45,false,true,"museo");
			disTxt= starHelper.addTextField(this,"0",40,bg.width/2,bg.height/2 + 20,Constants.DARK_COLOR,200,45,false,true,"museo");
			//disTxt.hAlign = HAlign.RIGHT;
			
			
			isBestScoreEnabled=true;
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY);
			if(!localStorageObj)
			{
				localStorageObj = new Object();
				localStorageObj.bestscore = distance;
				localStorageObj.bestscorepos= 0;
				bestscore = localStorageObj.bestscore;
				LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY,localStorageObj);
			}
			else
			{
				bestscore = localStorageObj.bestscore;
				trace(bestscore);
			}
			
			
		}
		
		
		public function update(offset:Number=1):void
		{
			/*
			for (var i:int = 0; i < offset; i++) 
			{
				setTimeout(addScore,0.03*i*1000);
			}
			*/
			
			addScore();
			
		}
		
		private function addScore(e:*=null):void
		{
			distance++;
			if(disTxt)disTxt.text = String(distance);
			
		}
		
		public function updateBestScore(heroPosX:Number):void
		{
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY);
			
			if(!localStorageObj)
			{
				localStorageObj = new Object();
				localStorageObj.bestscore = distance;
				LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY,localStorageObj);
			}
			
			if(localStorageObj && distance > Number(localStorageObj.bestscore) || !localStorageObj.hasOwnProperty('bestscore'))
			{
				localStorageObj.bestscore = distance;
				LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY,localStorageObj);
				
			}
			
			bestscore = localStorageObj.bestscore;
			isBestScoreEnabled=true;
		}
		
		public function remove(e:*=null):void
		{
			starHelper.removeAll(this);
			if(disTxt)disTxt=null;
			
		}
		
		
	}
}