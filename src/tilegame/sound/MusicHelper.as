﻿package tilegame.sound
{
	import com.developmentarc.core.datastructures.utils.HashTable;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	import tilegame.Constants;
	
	import treefortress.sound.SoundAS;
	import treefortress.sound.SoundInstance;
	import treefortress.sound.SoundManager;
	
	/**
	 * ...
	 * @author ankit
	 */
	public class MusicHelper extends Sprite
	{
		//for loop music 
		private static var musicManager:SoundManager = new SoundManager();
		private static var musicDic:HashTable= new HashTable();
		
		///////////////////////Music loops///////////////////////
		public static var MENU:String = "tg.menu";
		
		public function MusicHelper() 
		{
			
		}
		
		public static function init():void
		{
			
		}
		
		private static function addSound(sname:String):void
		{
			var soundClass:Class = getDefinitionByName(sname) as Class; 
			var sound:Sound = (new soundClass()) as Sound;
			musicManager.addSound(sname, sound);
			
		}
		
		public static function play(sname:String,loops:int=1,volume:Number=1,allowInterept:Boolean=false):void
		{
			if(!musicDic.getItem(sname))
			{
				musicDic.addItem(sname,sname);
				addSound(sname);
			}
			
			if(Constants.musicFlag){
				musicManager.play(sname,volume,0,-1,false,allowInterept);
				musicManager.fadeFrom(sname,0.4,1,60*5000);
			}
			
		}
		
		
		
		public static function pause(sname:String):void
		{
			if(musicDic.containsItem(sname))musicManager.pause(sname);
		}
		
		public static function resume(sname:String):void
		{
			if(Constants.musicFlag)musicManager.resume(sname);
		}
		
		public static function pauseAll():void
		{
			musicManager.pauseAll();
		}
		
		public static function resumeAll():void
		{
			if(Constants.musicFlag)musicManager.resumeAll();
		}
		
		public static function stop(sname:String):void
		{
			if(musicDic.containsItem(sname))
			{
				var snd:SoundInstance =musicManager.getSound(sname);
				snd.stop();
			}
		}
		
		
		public static function stopAll():void
		{
			musicManager.stopAll();
		}
		
		
	}
	
}