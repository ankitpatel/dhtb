﻿package tilegame.sound 
{
	import com.developmentarc.core.datastructures.utils.HashTable;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	import tilegame.Constants;
	
	import treefortress.sound.SoundAS;
	import treefortress.sound.SoundInstance;
	import treefortress.sound.SoundManager;

	/**
	 * ...
	 * @author ankit
	 */
	public class SoundHelper extends Sprite
	{
		//for small sounds
		private static var soundManager:SoundManager = new SoundManager();
		private static var soundDic:HashTable= new HashTable();
		///////////////////////sounds///////////////////////
		public static var HEROHIT:String = "tg.herohit";
		public static var SLIDE:String = "tg.slide";
		
		public function SoundHelper() 
		{
			
		}
		
		public static function init():void
		{
			
		}
		
		private static function addSound(sname:String):void
		{
			var soundClass:Class = getDefinitionByName(sname) as Class; 
			var sound:Sound = (new soundClass()) as Sound;
			soundManager.addSound(sname, sound);
			
		}
		
		public static function play(sname:String,loops:int=1,volume:Number=1,allowInterept:Boolean=false,fade:Boolean=false):void
		{
			if(!soundDic.getItem(sname))
			{
				soundDic.addItem(sname,sname);
				addSound(sname);
			}
			
			if(Constants.soundFlag){
				soundManager.playFx(sname,volume);
				
			}
			
		}
		
		
		public static function pause(sname:String):void
		{
			soundManager.pause(sname);
		}
		
		public static function resume(sname:String):void
		{
			if(Constants.soundFlag)soundManager.resume(sname);
		}
		
		public static function pauseAll():void
		{
			soundManager.pauseAll();
		}
		
		public static function resumeAll():void
		{
			soundManager.resumeAll();
		}
		
		public static function stop(sname:String):void
		{
			var snd:SoundInstance =soundManager.getSound(sname);
			snd.stop();
		}
		
		
		public static function stopAll():void
		{
			soundManager.stopAll();
		}
		
		public static function removeAll():void
		{
			soundManager.removeAll();
		}
	}

}