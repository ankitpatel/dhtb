﻿package tilegame.states
{
	import citrus.core.starling.StarlingState;
	import citrus.objects.CitrusSprite;
	import citrus.view.starlingview.AnimationSequence;
	import citrus.view.starlingview.StarlingCamera;
	
	import com.developmentarc.core.datastructures.utils.HashTable;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.playpower.logger.utils.setTimeout;
	if(CONFIG::AIR){
		import flash.desktop.NativeApplication;
	}
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.extensions.particles.PDParticleSystem;
	import starling.text.TextField;
	
	import tilegame.CitrusGame;
	import tilegame.Constants;
	import tilegame.components.BackgroundView;
	import tilegame.components.HeroSprite;
	import tilegame.components.LocalStorage;
	import tilegame.components.MobileInput;
	import tilegame.components.ObjectView;
	import tilegame.components.PauseScreen_VC;
	import tilegame.components.ScoreBar;
	import tilegame.components.TileView;
	import tilegame.components.TimerHelper;
	import tilegame.components.TutorialScreen;
	import tilegame.helpers.CustomEvent;
	import tilegame.sound.MusicHelper;
	import tilegame.sound.SoundHelper;
	import tilegame.star.Assets;
	import tilegame.star.StarEventHelper;
	import tilegame.star.starHelper;
	
	public class GameState extends StarlingState
	{
		static public var LEVEL_WIDTH:Number=768;  
		static public var LEVEL_HEIGHT:Number=10000000000000000;  
		
		protected var level:MovieClip;
		public var hero:HeroSprite;
		protected var _mobileInput:MobileInput;
		protected var flying:Boolean;
		
		/*
		[Embed(source="../../FG/assets/bubbles.pex", mimeType="application/octet-stream")]
		public static var bubblesPex:Class;
		
		[Embed(source="../../FG/assets/bubbles.png")]
		public static var bubblesPng:Class;
		*/
		
		protected var heroBubbles:CitrusSprite;
		protected var bubblesSprite:CitrusSprite;
		
		protected var pauseBtn:*;
		protected var boostBtn:*;
		protected var prevScore:Number=0;
		protected var pauseScreen:PauseScreen_VC;
		
		protected var infoTxt:TextField;
		public static var gameEnd:Boolean=false;
		public var wallHash:HashTable= new HashTable();
		
		protected var bestMC:Image;
		public var scoreBar:ScoreBar;
		protected var backgroundView:BackgroundView;
		protected var tileView:TileView;
		protected var objectView:ObjectView;
		private var scoreCounter:Number=0;
		private var starView:Sprite;
		private var isFirstTime:Boolean;
		private var fixedUpdateCount:int=0;
		private var enableWallBubbles:Boolean=false;
		private var tutView:TutorialScreen;
		private static var isPaused:Boolean= false;
		
		private var tutScreen:TutorialScreen;
		private static var isActive:Boolean=true;
		
		public function GameState(_isFirstTime:Boolean=false)
		{
			super();
			gameEnd =false;
			isActive = true;
			activate();
			isFirstTime= _isFirstTime;
			wallHash = new HashTable();
			
			if(CONFIG::AIR)
			{
				NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,onGameActivate);
				NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,onGameDeActivate);
			}
		}
		
		private function onGameActivate(e:*=null):void
		{
			if(tutScreen)
			{
				deactivate();
			}
			
		}
		
		private function onGameDeActivate(e:*=null):void
		{
			deactivate();
		}
		
		override public function initialize():void
		{
			super.initialize();
			//trace("GameState.initialize()??????????????????????????");
			starView = new Sprite();
			addChild(starView);
			
			scoreBar= new ScoreBar();
			starHelper.setPos(scoreBar,555,10,[70,-70]);
			scoreBar.alpha =9;
			starView.addChild(scoreBar);
			TweenLite.from([scoreBar], 0.3,{alpha:0,y:"-=150",ease:Back.easeOut}),
			objectView = new ObjectView(this);
			addHero();
			
		}
		
		private function addHero(e:*=null):void
		{
			
			hero = new HeroSprite('hero',{group:5,view:starHelper.getImage('hero0000',Assets.LEVEL_ATLAS)});
			if(LocalStorage.PLAYER_KEY == Constants.EASY)
			{
				starHelper.setPos(hero,objectView.posXArr[hero.curTrack] + hero.view.width/2 + 25, 650);
				
			}
			else if(LocalStorage.PLAYER_KEY == Constants.MEDIUM)
			{
				starHelper.setPos(hero,objectView.posXArr[hero.curTrack] + hero.view.width/2 + 15, 650);
				
			}
			else if(LocalStorage.PLAYER_KEY == Constants.HARD)
			{
				starHelper.setPos(hero,objectView.posXArr[hero.curTrack] + hero.view.width/2 + 5, 650);
				
			}
			
			add(hero);
			hero.onPause.add(onHeroDie);
			setupCamera();
			
			TweenLite.from(hero.view,0.8,{alpha:0,y:"+=200",onComplete:isFirstTime ? startTutorial : startGame});
			
			
		}
		
		private function startTutorial(e:*=null):void
		{
			deactivate();
			tutScreen = new TutorialScreen();
			tutScreen.addEventListener(TutorialScreen.ON_NEXT,startGame);
			starView.addChild(tutScreen);
		}
		
		private function startGame(e:*=null):void
		{
			if(tutScreen)
			{
				starHelper.remove(tutScreen);
				tutScreen=null;
			}
			
			hero.addTouchEvents();
			activate();
		}
		
		protected function onHeroDie(e:*=null):void
		{
			MusicHelper.pause(MusicHelper.MENU);
			
			gameEnd =true;
			_ce.playing=false;
			
			TweenMax.killAll();
			scoreBar.updateBestScore(hero.x);
			removeAll();
			Constants.citrusRef.state.destroy();
			Constants.citrusRef.state =null;
			
			Constants.citrusRef.state = new ScoreState();
			
		}
		
		private function addPauseScreen(e:*=null):void
		{
			pauseScreen = new PauseScreen_VC(PauseScreen_VC.ON_END);
			starView.addChild(pauseScreen);
			pauseScreen.addEventListener(PauseScreen_VC.ON_MENU,onMenuBtnClick);
			pauseScreen.addEventListener(PauseScreen_VC.ON_REPLAY,onReplayBtnClick);
		}
		
		protected function updateInfoText(text:String):void
		{
			infoTxt.text = text;
			if(text == "Let's GO!")
			{
				setTimeout(removeInfoTxt,1000);
				
			}
		}
		
		protected function removeInfoTxt(e:*=null):void
		{
			if(infoTxt){
				starHelper.remove(infoTxt);
				infoTxt=null;
			}
		}
		
		protected function onMenuBtnClick(e:*=null):void
		{
			// TODO Auto Generated method stub
			removePauseScreen();
			
		}
		
		protected function onReplayBtnClick(e:*=null):void
		{
			//trace("GameState.onReplayBtnClick(e)");
			
			// TODO Auto Generated method stub
			removePauseScreen();
			restartLevel();	
			
			activate();
			
		}
		
		protected function removePauseScreen(e:*=null):void
		{
			if(pauseScreen)
			{
				isPaused = false;
				pauseScreen.removeEventListeners();
				starHelper.remove(pauseScreen);
				pauseScreen=null;
			}
		}
		
		
		public function activate(e:*=null):void
		{
			//trace("GameState.activate(e)");
			TweenMax.resumeAll();
			if(objectView)objectView.activate();
			_ce.playing=true;
			MusicHelper.play(MusicHelper.MENU);
			
		}
		
		public function deactivate(e:*=null):void
		{
			//trace("GameState.deactivate(e)");
			TweenMax.pauseAll();
			if(objectView)objectView.deactivate();
			TimerHelper.pauseAllTimers();
			_ce.playing=false;
			MusicHelper.pause(MusicHelper.MENU);
			
		}
		
		
		protected function setupCamera(e:*=null):void
		{
			CitrusGame._camera = view.camera as StarlingCamera; // a reference for future use
			CitrusGame._camera.setUp(hero,null,new Point(1,0.6));
			CitrusGame._camera.bounds = new Rectangle(0,-LEVEL_HEIGHT + 1024,Constants.DEVICE_WIDTH,LEVEL_HEIGHT);
			CitrusGame._camera.allowRotation = true;
			CitrusGame._camera.allowZoom = true;
			CitrusGame._camera.easing.setTo(1, 1);
			CitrusGame._camera.rotationEasing = 1;
			CitrusGame._camera.zoomEasing = 1;	
			CitrusGame._camera.zoomFit(Constants.DEVICE_WIDTH,Constants.DEVICE_HEIGHT,true);
			CitrusGame._camera.reset();
			
		}
		
		public function fixedUpdate(e:*=null):void
		{
			//trace("GameState.fixedUpdate(e)");
			scoreCounter++;
			if(scoreCounter == 50)
			{
				scoreCounter=0;
			}
			//if(backgroundView)backgroundView.update(hero.y);
			var wallElements:Array = wallHash.getAllKeys();
			var wallLength:Number = wallElements.length;
			var onTrack:Boolean=false;
			
			for (var k:int = 0; k < wallLength; k++) 
			{
				var wallObj:*= wallElements[k];
				if(!wallObj)continue;
				var heroArt:* = view.getArt(hero);
				var objArt:* = view.getArt(wallObj);
				
				if(heroArt && objArt && wallObj.name.indexOf('wall') != -1)
				{
					var heroRect:Rectangle = heroArt.getBounds(heroArt.parent);
					var objRect:Rectangle = objArt.getBounds(objArt.parent);
					heroRect.top+= heroRect.height * 0.3;
					heroRect.bottom -=heroRect.height * 0.3;
					heroRect.left+=heroRect.width * 0.3;
					heroRect.right-=heroRect.width * 0.3;
					
					if(objRect.intersects(heroRect))
					{
						hero.onEnemyHit(wallObj);
					}
				}
				
				if(wallObj)
				{
					if( Math.abs(hero.y) - (Math.abs(wallObj.y) + wallObj.view.height) > Constants.DEVICE_HEIGHT/2.5)
					{	
						trace('remove >',wallObj.name);
						wallHash.remove(wallObj);
						wallObj.kill = true;
					}
					
				}
			}
			
			if(heroBubbles)
			{
				(heroBubbles.view as PDParticleSystem).emitterX = hero.x -80;
				(heroBubbles.view as PDParticleSystem).emitterY = hero.y + 20;
			}
			
			
		}
		
		
		private function removeWallParticle(e:*=null):void
		{
			//trace("GameState.removeParticle(e)");
			(bubblesSprite.view as PDParticleSystem).stop();
			removeChild((bubblesSprite.view as PDParticleSystem), true);
			bubblesSprite.kill=true;
			bubblesSprite=null;
			
			enableWallBubbles = false;
			
		}
		
		override public function update(timeDelta:Number):void
		{
			super.update(timeDelta);
			//("GameState.update(timeDelta)",gameEnd);
			fixedUpdate();
			
		}
		
		protected function restartLevel(e:*=null):void {
			
			scoreBar.updateBestScore(hero.x);
			removeAll();
			
			if(Constants.citrusRef.state)
			{
				Constants.citrusRef.state.destroy();
				Constants.citrusRef.state=null;
			}
			
			Constants.citrusRef.state = new GameState(false);
			
		}
		
		private function removeAll(e:*=true):void
		{
			if(objectView){
				objectView.deactivate();
			}
			
			
			if(backgroundView)
			{
				backgroundView.remove();
			}
			
			if(hero)
			{
				hero.kill=true;
				hero.destroy();
				remove(hero);
			}
			if(wallHash)
			{
				var wallKeys:Array = wallHash.getAllKeys();
				for (var k:int = 0; k < wallKeys.length; k++) 
				{
					var wallObj:*= wallKeys[k];
					if(wallObj.hasOwnProperty('updateCallEnabled'))wallObj.updateCallEnabled=false;
					//trace('rm wallObj',wallObj.name);
					remove(wallObj);
					wallObj.kill=true;
					wallObj.destroy();
					wallHash.remove(wallObj);
					wallObj=null;
					
					
				}
				
				wallHash.removeAll();
			}
			killAllObjects();
			removePauseScreen();
			
			if(starView)
			{
				starHelper.removeAll(starView);
				starView.removeFromParent(true);
				starView=null;
			}
			
		}
		
		override public function destroy():void{
			super.destroy();
			removeAll();
			
		}
		
	}
}