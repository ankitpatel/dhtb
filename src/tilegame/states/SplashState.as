﻿package tilegame.states
{
	import citrus.core.starling.StarlingState;
	import citrus.objects.CitrusSprite;
	
	import com.adobe.promotion.AdViewer;
	import com.adobe.promotion.AdViewerEvent;
	import com.greensock.TimelineMax;
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	
	import flash.events.Event;
	
	import starling.display.Image;
	import starling.text.TextField;
	
	import tilegame.Constants;
	import tilegame.components.BackgroundView;
	import tilegame.components.FlatButton;
	import tilegame.components.GameModeButton;
	import tilegame.components.LocalStorage;
	import tilegame.helpers.AdViewerHelper;
	import tilegame.helpers.GamesHelper;
	import tilegame.helpers.Singlebanner;
	import tilegame.sound.MusicHelper;
	import tilegame.star.Assets;
	import tilegame.star.starHelper;
	
	public class SplashState extends StarlingState
	{
		public static const ON_PLAY:String = "ON_PLAY";
		
		private var admob:*;
		private var splashTweenEffect:TimelineMax;
		
		private var easyBtn:GameModeButton;
		
		private var mediumBtn:GameModeButton;
		
		private var hardBtn:GameModeButton;
		
		private var rateBtn:GameModeButton;
		private var musicBtn:GameModeButton;
		
		private var titleTxt:TextField;

		private var attemptTxt:TextField;

		public function SplashState()
		{
			
		}
		
		override public function initialize():void
		{
			super.initialize();
			addSplashView();
		}
		
		public function addSplashView() {
			// constructor code
			//var backgroundView = new BackgroundView(this);
			//this.addChild(backgroundView);
			
			var easyBest:String = 'BEST: ' + LocalStorage.getObject(LocalStorage.PLAYER_DATA,Constants.EASY).bestscore;
			var mediumBest:String = 'BEST: ' + LocalStorage.getObject(LocalStorage.PLAYER_DATA,Constants.MEDIUM).bestscore;
			var hardBest:String = 'BEST: ' + LocalStorage.getObject(LocalStorage.PLAYER_DATA,Constants.HARD).bestscore;
			
			titleTxt = starHelper.addTextField(this,"Don't Touch\n The Bush",80,Constants.DEVICE_WIDTH/2,250,Constants.LIGHT_COLOR,700,200,false,true);
			
			easyBtn = new GameModeButton(this,Constants.EASY,easyBest,200,110,onClickEasyBtn);
			starHelper.setPos(easyBtn,Constants.DEVICE_WIDTH/2 -220,Constants.DEVICE_HEIGHT/2 - 30);
			
			mediumBtn = new GameModeButton(this,Constants.MEDIUM,mediumBest,200,110,onClickMediumBtn);
			starHelper.setPos(mediumBtn,Constants.DEVICE_WIDTH/2,Constants.DEVICE_HEIGHT/2 - 30);
			
			hardBtn = new GameModeButton(this,Constants.HARD,hardBest,200,110,onClickHardBtn);
			starHelper.setPos(hardBtn,Constants.DEVICE_WIDTH/2 +220,Constants.DEVICE_HEIGHT/2 - 30);
			
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.SETTING_KEY);
			if(!localStorageObj)
			{
				localStorageObj = new Object();
				localStorageObj.music = 'ON';
				LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.SETTING_KEY,localStorageObj);
			}
			
			localStorageObj.music == 'ON' ? enableMusic() : disableMusic();
			musicBtn = new GameModeButton(this,'MUSIC',localStorageObj.music,200,110,onClickMusicBtn);
			starHelper.setPos(musicBtn,Constants.DEVICE_WIDTH/2 - 110,Constants.DEVICE_HEIGHT/2+ 120);
			
			
			rateBtn = new GameModeButton(this,'RATE','APP',200,110,onClickRate);
			starHelper.setPos(rateBtn,Constants.DEVICE_WIDTH/2 + 110,Constants.DEVICE_HEIGHT/2+ 120);
			
			var attemptStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.ATTEMPT_KEY);
			attemptTxt = starHelper.addTextField(this,"GAMES PLAYED : "+attemptStorageObj.attempts ,40,Constants.DEVICE_WIDTH/2,Constants.DEVICE_HEIGHT/2+ 250,Constants.LIGHT_COLOR,700,200,false,true);
			
			if(CONFIG::AIR)
			{
				//addBannerAddvert();
				addAdobeAddViewer();
			}
			
			startTweenEffect();
			
		}
		
		public function startTweenEffect(e:*=null):void
		{
			splashTweenEffect = new TimelineMax({onComplete:starHelper.enableTouch});
			splashTweenEffect.insertMultiple( 
				[	TweenLite.from([titleTxt], 0.35,{alpha:0,y:"-=250",ease:Back.easeOut}),
					TweenLite.from([easyBtn,mediumBtn,hardBtn], 0.45,{alpha:0,x:"-=350",ease:Back.easeOut}),
					TweenLite.from([rateBtn,musicBtn], 0.45,{alpha:0,x:"+=350",ease:Back.easeOut}),
					TweenLite.from([attemptTxt], 0.35,{alpha:0,y:"+=250",ease:Back.easeOut}),
					
				],0.1,"normal",0.2);
			
		}
		
		
		
		public function endTweenEffect(callback:Function):void
		{
			splashTweenEffect.reverse();
			TweenLite.delayedCall(1,callback);
		}
		
		private function onClickMusicBtn(e:*=null):void
		{
			switch(musicBtn.text2)
			{
				case 'ON':
				{
					musicBtn.text2 = 'OFF';
					disableMusic();
					break;
				}
					
				case 'OFF':
				{
					musicBtn.text2 = 'ON';
					enableMusic();
					break;
				}
			}
		}
		
		private function enableMusic():void
		{
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.SETTING_KEY);
			Constants.musicFlag = true;
			Constants.soundFlag = true;
			localStorageObj.music = 'ON';
			LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.SETTING_KEY,localStorageObj);
			
		}
		
		private function disableMusic():void
		{
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.SETTING_KEY);
			Constants.musicFlag = false;
			Constants.soundFlag = false;
			MusicHelper.pauseAll();
			localStorageObj.music = 'OFF';
			LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.SETTING_KEY,localStorageObj);
			
			
		}
		
		if(CONFIG::AIR)
		{
			
			private function addBannerAddvert(e:*=null):void
			{
				trace("SplashState.addBannerAddvert(e)");
				var singleBanner:Singlebanner = new Singlebanner();
				Constants.stageRef.addChild(singleBanner);
				
			}
			
			private function addAdobeAddViewer():void
			{
				Constants.adViewHelper = new AdViewerHelper();
			}
			
			
		}
		
		
		private function onClickAchievement(e:*=null):void
		{
			//GSHelper.showAchievements();
		}
		
		private function onClickEasyBtn(e:*=null):void
		{
			// TODO Auto Generated method stub
			LocalStorage.PLAYER_KEY = Constants.EASY;
			endTweenEffect(startGameState);
		}
		
		private function onClickMediumBtn(e:*=null):void
		{
			// TODO Auto Generated method stub
			LocalStorage.PLAYER_KEY = Constants.MEDIUM;
			endTweenEffect(startGameState);
		}
		
		private function onClickHardBtn(e:*=null):void
		{
			// TODO Auto Generated method stub
			LocalStorage.PLAYER_KEY = Constants.HARD;
			endTweenEffect(startGameState);
		}
		
		private function onClickRate(e:*=null):void
		{
			// TODO Auto Generated method stub
			Constants.ratingBox.showRatingDialog();
		}		
		
		private function startGameState(e:*=null):void 
		{
			//start game state
			Constants.citrusRef.state = new GameState(true);
		}
		
		
		
	}
}