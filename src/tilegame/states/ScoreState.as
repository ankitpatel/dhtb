﻿package tilegame.states
{
	import citrus.core.starling.StarlingState;
	
	import com.greensock.TimelineMax;
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;
	
	import flash.external.ExternalInterface;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import tilegame.Constants;
	import tilegame.components.BackgroundView;
	import tilegame.components.FlatButton;
	import tilegame.components.LocalStorage;
	import tilegame.components.ScoreBar;
	import tilegame.helpers.AirFacebookSample;
	import tilegame.star.starHelper;
	
	public class ScoreState extends StarlingState
	{
		public static const ON_PLAY:String = "ON_PLAY";
		private var scoreTweenEffect:TimelineMax;
		
		private var scorecard:Sprite;
		
		private var shareBtn:FlatButton;
		
		private var tryAgainBtn:FlatButton;
		
		private var homeBtn:FlatButton;
		
		private var titleTxt:TextField;
		
		public function ScoreState()
		{
			
		}
		
		override public function initialize():void
		{
			super.initialize();
			addScoreView();
		}
		
		public function addScoreView() {
			// constructor code
			
			//var backgroundView = new BackgroundView(this);
			//this.addChild(backgroundView);
			
			
			var localStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.PLAYER_KEY);
			
			var attemptStorageObj:Object = LocalStorage.getObject(LocalStorage.PLAYER_DATA,LocalStorage.ATTEMPT_KEY);
			attemptStorageObj.attempts += 1;
			LocalStorage.addObject(LocalStorage.PLAYER_DATA,LocalStorage.ATTEMPT_KEY,attemptStorageObj);
			
			titleTxt = starHelper.addTextField(this,"Game Over!",80,Constants.DEVICE_WIDTH/2,200,Constants.LIGHT_COLOR,700,200,false,true);
			
			scorecard = new Sprite();
			var scoreBox:FlatButton = new FlatButton(scorecard,null,250,220);
			starHelper.setPos(scoreBox,0,80);
			
			var scoreTxtView:Sprite = new Sprite();
			scorecard.addChild(scoreTxtView);
			
			starHelper.addTextField(scoreTxtView,"Score",30,0,0,Constants.DARK_COLOR,300,40,false,true);
			starHelper.addTextField(scoreTxtView,ScoreBar.distance+'',50,0,50,Constants.DARK_COLOR,300,60,false,true);
			starHelper.addTextField(scoreTxtView,"Best",30,0,100,Constants.DARK_COLOR,300,40,false,true);
			starHelper.addTextField(scoreTxtView,localStorageObj.bestscore,50,0,150,Constants.DARK_COLOR,300,60,false,true);
			
			this.addChild(scorecard);
			starHelper.setPos(scorecard,Constants.DEVICE_WIDTH/2,300);
			
			shareBtn = new FlatButton(this,'SHARE',250,50,onClickShareBtn);
			starHelper.setPos(shareBtn,Constants.DEVICE_WIDTH/2,scorecard.y+scorecard.height + 30);
			
			tryAgainBtn = new FlatButton(this,'TRY AGAIN',250,50,onClickTraAgain);
			starHelper.setPos(tryAgainBtn,Constants.DEVICE_WIDTH/2,shareBtn.y+ 80);
			
			homeBtn = new FlatButton(this,'BACK HOME',250,50,onClickHomeBtn);
			starHelper.setPos(homeBtn,Constants.DEVICE_WIDTH/2,tryAgainBtn.y+ 80);
			
			
			//reportScore();
			
			startTweenEffect();
			
		}
		/*
		private function reportScore(e:*=null):void
		{
		switch(LocalStorage.PLAYER_KEY)
		{
		case Constants.EASY:
		{
		GSHelper.reportScore(GSHelper.NORMAL_LEADERBOARD_ID,ScoreBar.distance);
		if(ScoreBar.distance >= 100)
		{
		GSHelper.reportAchievement(GSHelper.ACHIV_NORMAL_100);
		}
		else if(ScoreBar.distance >= 50)
		{
		GSHelper.reportAchievement(GSHelper.ACHIV_NORMAL_50);
		}
		break;
		}
		
		case Constants.MEDIUM:
		{
		GSHelper.reportScore(GSHelper.FASTER_LEADERBOARD_ID,ScoreBar.distance);
		if(ScoreBar.distance >= 50)
		{
		GSHelper.reportAchievement(GSHelper.ACHIV_FASTER_50);
		}
		else if(ScoreBar.distance >= 30)
		{
		GSHelper.reportAchievement(GSHelper.ACHIV_FASTER_30);
		}
		break;
		}
		
		case Constants.HARD:
		{
		GSHelper.reportScore(GSHelper.PRO_LEADERBOARD_ID,ScoreBar.distance);
		if(ScoreBar.distance >= 30)
		{
		GSHelper.reportAchievement(GSHelper.ACHIV_PRO_30);
		}
		break;
		}
		}
		
		}*/
		
		public function startTweenEffect(e:*=null):void
		{
			scoreTweenEffect = new TimelineMax({onComplete:onCompleteTweenEffect});
			scoreTweenEffect.insertMultiple( 
				[	TweenLite.from([titleTxt], 0.3,{alpha:0,y:"-=250",ease:Back.easeOut}),
					TweenLite.from([scorecard,tryAgainBtn,homeBtn,shareBtn], 0.25,{alpha:0,y:"+=250",ease:Back.easeOut}),
					
				],0.1,"normal",0.3);
			
			
		}
		
		private function onCompleteTweenEffect(e:*=null):void
		{
			starHelper.enableTouch();
			if(CONFIG::AIR){
				if(Constants.adViewHelper.adViewer)Constants.adViewHelper.adViewer.load();
			}
		}
		
		public function endTweenEffect(callback:Function):void
		{
			scoreTweenEffect.reverse();
			TweenLite.delayedCall(1,callback);
		}
		
		
		private function onClickShareBtn(e:*=null):void 
		{
			trace("ScoreState.onClickShareBtn(e)");
			if(CONFIG::AIR)
			{
				var airFB :AirFacebookSample = new AirFacebookSample();
				airFB.onBtnShareLink("http://bit.ly/dhtb","Don't Hit The Bush",LocalStorage.PLAYER_KEY + "- High Score : "+ScoreBar.distance);
			}
			else
			{
				//for web call javascrip fun here
				ExternalInterface.call("onFBShareClick");
			}
		}
		
		private function onClickTraAgain(e:*=null):void 
		{
			endTweenEffect(startGameState);
			
		}
		
		private function onClickHomeBtn(e:*=null):void 
		{
			endTweenEffect(startSplashState);
		}
		
		private function startSplashState(e:*=null):void 
		{
			Constants.citrusRef.state = new SplashState();
		}
		
		private function startGameState(e:*=null):void 
		{
			//start game state
			Constants.citrusRef.state = new GameState(false);
		}
		
	}
}