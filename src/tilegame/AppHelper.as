﻿package tilegame
{
	import lib.flox.FloxHelper;
	import lib.rateus.RateBoxExample;
	
	import tilegame.helpers.FBHelper;
	import tilegame.helpers.GSHelper;
	import tilegame.helpers.GamesHelper;

	public class AppHelper
	{
		public function AppHelper()
		{
		}
		
		public static function init(e:*=null):void
		{
			
			if (CONFIG::AIR) 
			{
				//////INIT RATING EXTENTION///////////////////
				Constants.ratingBox= new RateBoxExample();
				Constants.stageRef.addChild(Constants.ratingBox);
			
				//init FLOX//
				FloxHelper.init();
				
			}
		}
	}
}