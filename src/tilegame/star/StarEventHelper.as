﻿package tilegame.star
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class StarEventHelper
	{
		private var eventPhase:String;
		private var eventHandler:Function;
		private var deleteHandler:Boolean;
		
		public static const TRIGGERED:String = Event.TRIGGERED;
		public static const BEGAN:String = TouchPhase.BEGAN;
		public static const MOVED:String = TouchPhase.MOVED;
		public static const ENDED:String = TouchPhase.ENDED;
		public static const HOVER:String = TouchPhase.HOVER;
		public static const BUTTON:String = "BUTTON"; //scale down on touch begin 
		public static const FRAME:String = "FRAME"; // change frame of moviclip on touch begin
		
		private var oriScaleX:*=null;
		private var oriScaleY:*=null;
		
		public function StarEventHelper(object:*,phase:String,handler:Function,delHandler:Boolean=false)
		{
			if(object)
			{
				eventPhase=phase;
				eventHandler=handler;
				deleteHandler= delHandler;
				
				switch(eventPhase)
				{
					case FRAME:
					{
						object.addEventListener(TouchEvent.TOUCH, onFrameTouch);
						break;
					}
					case BUTTON:
					{
						object.addEventListener(TouchEvent.TOUCH, onButtonTouch);
						break;
					}
						
					case TRIGGERED:
					{
						object.addEventListener(Event.TRIGGERED, onTriggerEvent);
						break;
					}
						
					default:
					{
						object.addEventListener(TouchEvent.TOUCH, onTouch);
						break;
					}
				}
			}
			
		}
		
		private function onTouch(e:TouchEvent):void
		{
			// TODO Auto Generated method stub
			var tch:Touch= e.getTouch(e.currentTarget as DisplayObject,eventPhase);
			if(tch){
				eventHandler(e);
				if(deleteHandler)e.currentTarget.removeEventListener(TouchEvent.TOUCH, onTouch);
				
			}
		}
		
		private function onTriggerEvent(e:*):void
		{
			eventHandler(e);
			if(deleteHandler){
				e.currentTarget.removeEventListener(Event.TRIGGERED, onTriggerEvent);
				e.currentTarget.removeEventListeners();
				
			}
			
		}
		
		public static function addTouchEventListener(object:*,phase:String,handler:Function,delHandler:Boolean=false):void
		{
			if(object)
			{
				new StarEventHelper(object,phase,handler,delHandler);
			}
		}
		
		public static function removeTouchEventListener(object:*,handler:Function):void
		{
			object.removeEventListener(TouchEvent.TOUCH, handler);
			object.removeEventListener(Event.TRIGGERED, handler);
			object.removeEventListeners();
		}
		
		private function onButtonTouch(e:*=null):void
		{
			var tch:Touch= e.getTouch(e.currentTarget as DisplayObject);
			var target:*=  e.currentTarget;
			if (tch)
			{
				switch(tch.phase)
				{
					case TouchPhase.BEGAN:
						
						if(!oriScaleX && !oriScaleY){
							oriScaleX=target.scaleX;
							oriScaleY=target.scaleY;
						}
						TweenMax.to(target,0.1,{scaleX:oriScaleX-0.03,scaleY:oriScaleY-0.03});
						break;
					case TouchPhase.MOVED:
						if(!target.bounds.containsPoint(new Point(tch.globalX,tch.globalY))) 
						{
							TweenLite.to(target,0.1,{scaleX:oriScaleX,scaleY:oriScaleY});
						}
						break;
					case TouchPhase.ENDED:
						var globalPoint:Point=new Point(tch.globalX,tch.globalY);
						var localPoint:Point=getLocalPoint(target,globalPoint);
						if(target.bounds.containsPoint(localPoint)) 
						{
							TweenLite.to(target,0.1,{scaleX:oriScaleX,scaleY:oriScaleY});
							eventHandler(e);
							if(deleteHandler)e.currentTarget.removeEventListener(TouchEvent.TOUCH, onTouch);
						}
						break;
					
				}
			}
		}
		
		public static function getLocalPoint(mc:*,globalPoint:Point):Point
		{
			var localPoint:Point= mc.parent.globalToLocal(globalPoint);
			return localPoint;
		}
		
		
		public static function getGlobalPoint(mc:*):Point
		{
			return mc.localToGlobal(new Point(0,0));
		}
		
		
		private function onFrameTouch(e:*=null):void
		{
			var tch:Touch= e.getTouch(e.currentTarget as DisplayObject);
			var target:*=  e.currentTarget;
			
			if (tch)
			{
				switch(tch.phase)
				{
					case TouchPhase.BEGAN:
						starHelper.gotoAndStop(target,1);
						break;
					case TouchPhase.MOVED:
						if(!target.bounds.containsPoint(new Point(tch.globalX,tch.globalY))) 
						{
							starHelper.gotoAndStop(target,0);
						}
						break;
					case TouchPhase.ENDED:
						var globalPoint:Point=new Point(tch.globalX,tch.globalY);
						var localPoint:Point=getLocalPoint(target,globalPoint);
						if(target.bounds.containsPoint(localPoint)) 
						{
							starHelper.gotoAndStop(target,0);
							eventHandler(e);
							if(deleteHandler)e.currentTarget.removeEventListener(TouchEvent.TOUCH, onTouch);
						}
						break;
					
				}
			}
		}
		
	}
}