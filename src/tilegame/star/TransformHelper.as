﻿package tilegame.star
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.plugins.ThrowPropsPlugin;
	
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class TransformHelper
	{
		private var container:*
		private var panFlag:Boolean;
		private var zoomFlag:Boolean;
		private var rotateFlag:Boolean;
		
		
		var t1:uint, t2:uint, y1:Number, y2:Number, x1:Number, x2:Number, xOverlap:Number, xOffset:Number, yOverlap:Number, yOffset:Number;
		private var direction:String;
		private var scrollTwn:TweenLite;
		private var bounds:Rectangle;
		
		
		
		public function TransformHelper(_container:*,isPan:Boolean=false,_panBounds:Rectangle=null,isZoom:Boolean=false,minScale:Number=0.5,maxScale:Number=1.5,isRotate:Boolean=false)
		{
			container=_container;
			panFlag = isPan;
			zoomFlag= isZoom;
			rotateFlag= isRotate;
			bounds=_panBounds;
			
			
			container.addEventListener(TouchEvent.TOUCH, onTouch);
			
			
		}
		
		private function onTouch(event:TouchEvent):void
		{
			/////BEGIN//////////
			var touch:Touch = event.getTouch(container, TouchPhase.BEGAN);
			if(touch)
			{
				if(container.scaleX !=1 && container.scaleY != 1)
				{
					TurnOffScrollPolicy();
				}
			}
			
			
			///////MOVE/////////
			
			var touches:Vector.<Touch> = event.getTouches(container, TouchPhase.MOVED);
			if (touches.length == 1)
			{
				// one finger touching -> move
				var delta:Point = touches[0].getMovement(container);
				
				if(panFlag)
				{
					container.x += delta.x;
					container.y += delta.y;
					
					
					if (container.y > bounds.top  ) {
						container.y = bounds.top ;
						
					} else if (container.y < bounds.top - yOverlap) {
						container.y = bounds.top - yOverlap;
						
					}
					
					if (container.x > bounds.left) {
						container.x = bounds.left ;
						
						
					} else if (container.x < bounds.left - xOverlap) {
						container.x = bounds.left - xOverlap ;
						
						
					} 
					
					
				}
			}            
			else if (touches.length == 2)
			{
				// two fingers touching -> rotate and scale
				var touchA:Touch = touches[0];
				var touchB:Touch = touches[1];
				
				var currentPosA:Point  = touchA.getLocation(container.parent);
				var previousPosA:Point = touchA.getPreviousLocation(container.parent);
				var currentPosB:Point  = touchB.getLocation(container.parent);
				var previousPosB:Point = touchB.getPreviousLocation(container.parent);
				
				var currentVector:Point  = currentPosA.subtract(currentPosB);
				var previousVector:Point = previousPosA.subtract(previousPosB);
				
				var currentAngle:Number  = Math.atan2(currentVector.y, currentVector.x);
				var previousAngle:Number = Math.atan2(previousVector.y, previousVector.x);
				var deltaAngle:Number = currentAngle - previousAngle;
				
				// update pivot point based on previous center
				var previousLocalA:Point  = touchA.getPreviousLocation(container);
				var previousLocalB:Point  = touchB.getPreviousLocation(container);
				container.pivotX = (previousLocalA.x + previousLocalB.x) * 0.5;
				container.pivotY = (previousLocalA.y + previousLocalB.y) * 0.5;
				// update location based on the current center
				container.x = (currentPosA.x + currentPosB.x) * 0.5;
				container.y = (currentPosA.y + currentPosB.y) * 0.5;
				
				// rotate
				if(rotateFlag)container.rotation += deltaAngle;
				
				// scale
				var sizeDiff:Number = currentVector.length / previousVector.length;
				if(zoomFlag)
				{
					container.scaleX *= sizeDiff;
					container.scaleY *= sizeDiff;
					
					
					if(container.scaleX < 0.5) {
						container.scaleX= container.scaleX = 0.5;
					}
					if(container.scaleY < 0.5) {
						container.scaleY= container.scaleY = 0.5;
					}
					
					if(container.scaleX > 1.5) {
						container.scaleX = 1.5;
					}
					if(container.scaleY > 1.5) {
						container.scaleY = 1.5;
					}
					
					
				}
			}
			
			////////////END///////////////
			
			var touch:Touch = event.getTouch(container, TouchPhase.ENDED);
			if(touch)
			{
				if(container.scaleX != 1 && container.scaleY != 1)
				{
					TweenMax.to(container,0.65,{scaleX:1,scaleY:1,ease:Strong.easeOut,onComplete:TurnOnScrollPolicy})
				}
			}
			
		}
		
		private function TurnOffScrollPolicy(e:*=null):void
		{
			if(container is ScrollContainer)
			{
				(container as ScrollContainer).verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
				(container as ScrollContainer).horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
			}
			
		}
		
		
		private function TurnOnScrollPolicy(e:*=null):void
		{
			if(container is ScrollContainer)
			{
				(container as ScrollContainer).verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
				(container as ScrollContainer).horizontalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			}
			
		}
		
		
		
		public function dispose():void
		{
			container.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
	}
}