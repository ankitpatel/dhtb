﻿package tilegame.star
{
	import adobe.utils.CustomActions;
	
	import com.gestouch.core.GestureState;
	import com.gestouch.events.GestureEvent;
	import com.gestouch.gestures.LongPressGesture;
	import com.gestouch.gestures.PanGesture;
	import com.gestouch.gestures.RotateGesture;
	import com.gestouch.gestures.SwipeGesture;
	import com.gestouch.gestures.SwipeGestureDirection;
	import com.gestouch.gestures.TapGesture;
	import com.gestouch.gestures.TransformGesture;
	import com.gestouch.gestures.ZoomGesture;
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	import starling.display.Sprite;
	
	
	
	
	public class GestureHelper extends Sprite
	{
		public static const SWIPE_RIGHT:String ="SWIPE_RIGHT";
		public static const SWIPE_LEFT:String ="SWIPE_LEFT";
		public static const SWIPE_UP:String ="SWIPE_UP";
		public static const SWIPE_DOWN:String ="SWIPE_DOWN";
		
		public static const SINGLE_TAP:String ="SWIPE_DOWN";
		public static const DOUBLE_TAP:String ="DOUBLE_TAP";
		
		public static const LONG_PRESS:String ="LONG_PRESS";
		
		public static const ZOOM_BEGAN:String ="ZOOM_BEGAN";
		public static const ZOOM_CHANGED:String ="ZOOM_CHANGED";
		public static const ZOOM_ENDED:String ="ZOOM_ENDED";
		
		
		public static const PAN:String ="PAN";
		public static const PAN_BEGAN:String ="PAN_BEGAN";
		public static const PAN_CHANGED:String ="PAN_CHANGED";
		public static const PAN_ENDED:String ="PAN_ENDED";
		
		private var targetMatrix:Matrix;
		private var targetScale:Number;

		private var swipeGesture:SwipeGesture;

		private var zoomGesture:ZoomGesture;

		private var rotateGesture:RotateGesture;

		private var panGesture:PanGesture;

		private var tapGesture:TapGesture;

		private var transformGesture:TransformGesture;
		
		public function GestureHelper()
		{
			
			/*
			////////TAP GESTURES//////////
			addTapGesture(splash,1);
			addTapGesture(splash,2);
			
			///////SWIPE GESTURES////////////
			addSwipeGesture(splash,SwipeGestureDirection.UP);
			addSwipeGesture(splash,SwipeGestureDirection.DOWN);
			addSwipeGesture(splash,SwipeGestureDirection.RIGHT);
			addSwipeGesture(splash,SwipeGestureDirection.LEFT);
			
			
			//////LONG PRESS GESTURE////////
			addLongPressGesture(splash,100,GestureEvent.GESTURE_BEGAN);
			addLongPressGesture(splash,1000,GestureEvent.GESTURE_ENDED);
			
			
			//////PAN GESTURE///////////////
			addPanGesture(splash);
			
			
			/////ROTATE GESTURE/////////////
			addRotateGesture(splash);
			
			/////Zoom GESTURE/////////////
			addZoomGesture(splash);
			
			/////Transform GESTURE/////////////
			addTransformGesture(splash);
			*/
		}
		
		public function addTapGesture(target:*,numTapsRequired:int=1,maxTapDelay:int=400):void
		{
			tapGesture = new TapGesture(target);
			tapGesture.numTapsRequired = numTapsRequired;
			tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture,false,0,true);
			tapGesture.maxTapDelay=maxTapDelay;
			
		}
		
		public function addLongPressGesture(target:*,minPressDuration:Number=1000,eventPhase:String=null,numTouchesRequired:int=1,slop:*=null):void
		{
			//eventPhase can be  GestureEvent.GESTURE_BEGAN or GestureEvent.GESTURE_ENDED
			if(!eventPhase)eventPhase= GestureEvent.GESTURE_BEGAN;
			
			var longPressGesture:LongPressGesture = new LongPressGesture(target);
			longPressGesture.addEventListener(eventPhase, onGesture,false,0,true);
			
			if(slop)longPressGesture.slop=slop;
			longPressGesture.numTouchesRequired=numTouchesRequired;
			longPressGesture.minPressDuration=minPressDuration;
		}
		
		public function addPanGesture(target:*,minTouchesRequired:Number=1,maxTouchesRequired:int=2):void
		{
			panGesture = new PanGesture(target);
			panGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onGesture,false,0,true);
			panGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onGesture,false,0,true);
			panGesture.minNumTouchesRequired=minTouchesRequired;
			panGesture.maxNumTouchesRequired=maxTouchesRequired;
		}
		
		
		public function addRotateGesture(target:*):void
		{
			rotateGesture = new RotateGesture(target);
			rotateGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onGesture,false,0,true);
			rotateGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onGesture,false,0,true);
			
		}
		
		public function addZoomGesture(target:*):void
		{
			zoomGesture = new ZoomGesture(target);
			zoomGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onGesture,false,0,true);
			zoomGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onGesture,false,0,true);
			zoomGesture.addEventListener(GestureEvent.GESTURE_ENDED, onGesture,false,0,true);
			
			
		}
		
		public function addTransformGesture(target:*):void
		{
			transformGesture = new TransformGesture(target);
			transformGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onGesture,false,0,true);
			transformGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onGesture,false,0,true);
		}
		
		
		public function addSwipeGesture(target:*,direction:*=null,minOffest:Number=150,numTouchesRequired:int=1,minVelocity:Number=2):void
		{
			swipeGesture = new SwipeGesture(target);
			swipeGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture,false,0,true);
			if(direction)
			{
				swipeGesture.direction= direction;
			}
			else
			{
				swipeGesture.direction=SwipeGestureDirection.RIGHT;
			}
			swipeGesture.minOffset = minOffest;
			swipeGesture.numTouchesRequired=numTouchesRequired;
			swipeGesture.minVelocity=minVelocity;
		}
		
		
		public function onGesture(event:GestureEvent=null):void
		{
			var gesture:* = event.target;
			var target:* = gesture.target;
			
			if(gesture is TapGesture)
			{
				if (gesture.numTapsRequired == 1)
				{
					//SINGLE TAP
					//splash.info.text="SingleTapGesture!";
					dispatchEvent(new StarlingEvent(SINGLE_TAP,target));
					
					
				}
				else if (gesture.numTapsRequired == 2)
				{
					//DOUBLE TAP
					//splash.info.text="DoubleTapGesture!";
					dispatchEvent(new StarlingEvent(DOUBLE_TAP,target));
					
				}
			}
			
			if(gesture is LongPressGesture)
			{
				//splash.info.text="Long Press Gesture!";
				dispatchEvent(new StarlingEvent(LONG_PRESS,target));
				
			}
			
			if(gesture is RotateGesture)
			{
				var matrix:Matrix = target.transform.matrix;
				var transformPoint:Point = matrix.transformPoint(target.globalToLocal(gesture.location));
				matrix.translate(-transformPoint.x, -transformPoint.y);
				matrix.rotate(gesture.rotation);
				matrix.translate(transformPoint.x, transformPoint.y);
				target.transform.matrix = matrix;
				
				//splash.info.text="Rotate Gesture!";
			}
			
			if(gesture is PanGesture)
			{
				//target.x += gesture.offsetX;
				//target.y += gesture.offsetY;
				var obj:Object= new Object();
				obj.target= target;
				obj.gesture= gesture;
				obj.offsetX= gesture.offsetX;
				obj.offsetY= gesture.offsetY;
				
				
				target.x += gesture.offsetX;
				target.y += gesture.offsetY;
				
				dispatchEvent(new StarlingEvent(PAN,obj));
				
				if(gesture.state == GestureState.BEGAN)
				{
					dispatchEvent(new StarlingEvent(PAN_BEGAN,obj));
				}
				else if(gesture.state == GestureState.CHANGED)
				{
					dispatchEvent(new StarlingEvent(PAN_CHANGED,obj));
				}
				else if(gesture.state == GestureState.ENDED)
				{
					dispatchEvent(new StarlingEvent(PAN_ENDED,obj));
					
				}
			}
			
			
			if(gesture is ZoomGesture)
			{
				trace('zoom',gesture.state,gesture.scaleX, gesture.scaleY);
				var obj:Object= new Object();
				obj.target= target;
				obj.gesture= gesture;
				
				
				var matrix:Matrix = target.transform.matrix;
				if(gesture.state == GestureState.BEGAN)
				{
					targetMatrix = matrix;
					targetScale = gesture.scaleX;
					dispatchEvent(new StarlingEvent(ZOOM_BEGAN,obj));
					
					trace("ZOOM_BEGAN");
				}
				else if(gesture.state == GestureState.CHANGED)
				{
					//if(gesture.scaleX >= targetScale &&  gesture.scaleX <= 1.1*targetScale)
					{
						var transformPoint:Point = matrix.transformPoint(target.globalToLocal(gesture.location));
						matrix.translate(-transformPoint.x, -transformPoint.y);
						matrix.scale(gesture.scaleX, gesture.scaleY);
						matrix.translate(transformPoint.x, transformPoint.y);
						target.transform.matrix = matrix;
					}
					dispatchEvent(new StarlingEvent(ZOOM_CHANGED,obj));
					
					trace("ZOOM_CHANGED");
				}
				else if(gesture.state == GestureState.ENDED)
				{
					//target.transform.matrix = targetMatrix;
					dispatchEvent(new StarlingEvent(ZOOM_ENDED,obj));
					trace("ZOOM_ENDED");
				}
				//splash.info.text="Zoom Gesture!";
			}
			
			if(gesture is TransformGesture)
			{
				var matrix:Matrix = target.transform.matrix;
				
				// Panning
				matrix.translate(gesture.offsetX, gesture.offsetY);
				target.transform.matrix = matrix;
				
				if (gesture.scale != 1 || gesture.rotation != 0)
				{
					// Scale and rotation.
					var transformPoint:Point = matrix.transformPoint(target.globalToLocal(gesture.location));
					matrix.translate(-transformPoint.x, -transformPoint.y);
					matrix.rotate(gesture.rotation);
					matrix.scale(gesture.scale, gesture.scale);
					matrix.translate(transformPoint.x, transformPoint.y);
					
					target.transform.matrix = matrix;
				}
				
				//splash.info.text="Transform Gesture!";
			}
			
			if(gesture is SwipeGesture)
			{
				trace("GestureHelper.onGesture(event)",(gesture as SwipeGesture).offsetX,(gesture as SwipeGesture).offsetY,(gesture as SwipeGesture).direction);
				
				var offsetX:Number=(gesture as SwipeGesture).offsetX;
				var offsetY:Number=(gesture as SwipeGesture).offsetY;
				
				if(offsetX > 0)
				{
					dispatchEvent(new StarlingEvent(SWIPE_RIGHT,target));
				}
				else if(offsetX <0)
				{
					dispatchEvent(new StarlingEvent(SWIPE_LEFT,target));
					
				}
				
				if(offsetY > 0)
				{
					dispatchEvent(new StarlingEvent(SWIPE_DOWN,target));
				}
				else if(offsetY <0)
				{
					dispatchEvent(new StarlingEvent(SWIPE_UP,target));
					
				}
				
			}
			
			
			
		}
		
		public function removeGesture(gesture:*):void
		{
			gesture.dispose();// all event listeners automatically removed
			gesture = null;
		}
		
		public function disposeAll(e:*=null):void
		{
			if(swipeGesture){
				
				swipeGesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED, onGesture);
				swipeGesture.dispose();	
				swipeGesture=null;	
			
			}
			
			if(zoomGesture){
				zoomGesture.dispose();	
				zoomGesture=null;
			
			}
			
			if(rotateGesture){
				rotateGesture.dispose();	
				rotateGesture=null;
			
			
			}
			
			if(panGesture){
				panGesture.dispose();	
				panGesture=null;
			
			}
			
			if(tapGesture){
				tapGesture.dispose();
				tapGesture=null;
			
			}
			
			if(transformGesture){
				transformGesture.dispose();
				transformGesture=null;
		
			}
			
			
		}
	}
}