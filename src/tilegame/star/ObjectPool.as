﻿package tilegame.star
{
	import citrus.core.CitrusEngine;
	
	import com.developmentarc.core.datastructures.utils.HashTable;
	
	import starling.display.DisplayObject;
	

	public class ObjectPool
	{
		public var pool:Array; //total number of items that can be added to stage
		private var counter:int;
		private var type:Class;
		private var name:String;
		private var params:Object;
		
		
		public function ObjectPool(_type:Class,_name:String,_params:Object,len:int)
		{
			this.type = _type;
			this.name = _name;
			this.params = _params;
				
			counter = len;
			
			init();
		}
		
		private function init(e:*=null):void
		{
			pool = new Array();
			for (var i:int = 0; i < counter; i++) 
			{
				pool.push(new type(name,params));
			}
			trace("ObjectPool.init(e)",pool.length);
			
		}
		
		
		public function getObject():*
		{
			if(!pool)init();
			
			if(counter > 0 && pool && pool.length)
			{ 
				var mc:*= pool.pop();
				trace("use object>>>>",mc.name,pool.length);
				return mc;
			}
			else
			{
				throw new Error("You exhausted the pool!");
			}
		}
		
		public function returnObject(s:*):void
		{
			if(s)CitrusEngine.getInstance().state.remove(s);
			if(pool){
				pool.push(s);
				trace("restore object>>>>",s.name,pool.length);
				
			}
			
			
		}
		
		public function destroy():void
		{
			trace(this,destroy);
			pool = null;
			counter=0;
		}
	}
}