﻿package tilegame.star
{
	
	
	import feathers.controls.Label;
	import feathers.controls.TextInput;
	import feathers.display.Scale9Image;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale9Textures;
	
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextFormatAlign;
	import flash.text.TextRenderer;
	
	import tilegame.Constants;
	
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.display.Stage;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.particles.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	/**
	 * ...
	 * @author ankit
	 */
	public class starHelper
	{
		
		public static var stageRef:Stage;//starling main stage=>Starling.current.stage
		private static var touchQuad:Quad;
		
		public function starHelper() 
		{
			
		}
		
		private static var callbackFunc:Function;
		static public const IMG:String = "Image";
		static public const MOVIECLIP:String = "MovieClip";
		static public const BUTTON:String = "Button";
		static public const TEXTFIELD:String = "TEXTFIELD";
		static public const TEXTINPUT:String = "TEXTINPUT";
		
		public static function addObject(parent:*,type:String,target:String=null,atlasName:String=null,posX:Number=0,posY:Number=0,fps:Number = 60,isTouchable:Boolean=true,setPivotFlag:Boolean=false,dwnState:String=null,loop:Boolean=false,xOffset:Array=null,yOffset:Array=null,aspectRatio:Boolean=false,aspectScale:*=null):*
		{
			var mc:*=null;
			if(type == IMG)
			{
				if(atlasName){
					mc= getImage(target+"0000",atlasName);
				}
				else
				{
					mc= getImage(target);
				}
				
				setPos(mc,posX,posY,xOffset,yOffset,aspectRatio,aspectScale);
				mc.touchable=isTouchable;
				parent.addChild(mc);
				
				
			}
			else if(type == BUTTON)
			{
				mc= getButton(atlasName,target+"0000",dwnState);
				setPos(mc,posX,posY,xOffset,yOffset,aspectRatio,aspectScale);
				mc.touchable=isTouchable;
				parent.addChild(mc);
				
			}
			else if(type == MOVIECLIP)
			{
				mc= getMovieClip(target,atlasName,fps,true,loop);
				setPos(mc,posX,posY,xOffset,yOffset,aspectRatio,aspectScale);
				mc.touchable=isTouchable;
				parent.addChild(mc);
				
			}
			else
			{
				throw new Error("UNDEFINED TYPE - inside starHelper->addObject");
			}
			
			if(mc.hasOwnProperty("name"))mc.name= target;
			if(mc && setPivotFlag)setPivot(mc);
			
			
			return mc;
		}
		
		
		
		public static function addTextField(parent:*,text:String="",size:Number=30,posX:Number=0,posY:Number=0,color:uint=0xffffff,width:Number=200,height:Number=55,isTouchable:Boolean=false,setPivotFlag:Boolean=true,font:String="museo",xOffset:Array=null,yOffset:Array=null,aspectRatio:Boolean=false,aspectScale:*=null):TextField
		{
			var txtField:TextField= getTextField(width,height,text,size,font,setPivotFlag,color,aspectRatio);
			setPos(txtField,posX,posY,xOffset,yOffset,aspectRatio,aspectScale);
			txtField.touchable=isTouchable;
			txtField.batchable = true;
			parent.addChild(txtField);
			
			return txtField;
			
			
			
		}
		
		public static function addLabel(parent:*,text:String="",size:Number=30,posX:Number=0,posY:Number=0,color:uint=0xffffff,width:Number=200,height:Number=55,isTouchable:Boolean=false,setPivotFlag:Boolean=true,font:String=null,xOffset:Array=null,yOffset:Array=null,aspectRatio:Boolean=false,aspectScale:*=null):*
		{
			var label:Label = new Label();
			label.width = starHelper.getHDXPos(width);
			label.height = starHelper.getHDYPos(height);
			label.textRendererProperties.textFormat = new BitmapFontTextFormat("museo",starHelper.getHDYPos(size),color,TextFormatAlign.LEFT);
			label.textRendererProperties.useSeparateBatch = false; 
			if(setPivotFlag)setPivot(label);
			setPos(label,posX,posY,xOffset,yOffset,aspectRatio,aspectScale);
			label.touchable=isTouchable;
			parent.addChild(label);
			
			return label;
			
			
		}
		
		public static function getScale9Image(imageName:String,atlas:String=null,_rect:Rectangle=null,width:Number=100,height:Number=100,touchable:Boolean=false):Scale9Image
		{
			var texture:Texture;
			if(atlas)
			{
				texture= Assets.getTextureAtlas(atlas).getTexture(imageName);
			}
			else
			{
				texture= Assets.getTexture(imageName);
			}
			var rect:Rectangle = (_rect == null) ?  new Rectangle( starHelper.getHDXPos(20), starHelper.getHDYPos(20), starHelper.getHDXPos(20), starHelper.getHDYPos(20) ) : _rect;
			var textures:Scale9Textures = new Scale9Textures( texture, rect );
			
			var image:Scale9Image = new Scale9Image( textures );
			image.width = starHelper.getHDXPos(width);
			image.height = starHelper.getHDYPos(height);
			image.touchable=touchable;
			return image;
		}
		
		public static function getTextInput(promt:String="",promtFont:String="museo",prompSize:int=30,promptColor:uint=0x333333,text:String=null,color:uint=0xffffff,size:Number=30,font:String="Dimbo",width:Number=300,height:Number=60,focus:Boolean=true,restrict:String=null,maxChars:int=100):TextInput
		{
			var txtField:TextInput = new TextInput();
			txtField.width=starHelper.getHDXPos(width);
			txtField.height=starHelper.getHDYPos(height);
			txtField.prompt=promt;
			if(text)txtField.text=text;
			txtField.paddingTop = starHelper.getHDYPos(-15);
			
			txtField.promptProperties.textFormat = new BitmapFontTextFormat( promtFont,starHelper.getHDYPos(prompSize),promptColor );
			txtField.textEditorProperties.embedFonts = true;
			txtField.textEditorProperties.fontFamily = font;
			txtField.textEditorProperties.fontName = font;
			txtField.textEditorProperties.fontSize = starHelper.getHDYPos(size);
			txtField.textEditorProperties.color = color;
			if(focus)txtField.setFocus();
			if(restrict)txtField.restrict = restrict;
			txtField.maxChars = maxChars;
			
			return txtField;
		}
		
		
		
		public static function getMovieClip(movieClipName:String,atlasName:String,fps:Number = 60,addToJugglerFlag:Boolean=true,loop:Boolean=false):MovieClip
		{
			var frames:Vector.<Texture> = Assets.getTextureAtlas(atlasName).getTextures(movieClipName);
			var mMovie:MovieClip = new MovieClip(frames, fps);
			if (addToJugglerFlag)
			{
				Starling.juggler.add(mMovie);
				
			}
			
			mMovie.loop=loop;
			mMovie.stop();
			
			return mMovie;
		}
		
		public static function setScale(mc:*,scale:Number,_setPivot:Boolean=false):void
		{
			if(_setPivot)setPivot(mc,null,null,true);
			mc.scaleX =scale;
			mc.scaleY =scale;
		}
		
		public static function gotoAndStop(mc:*,frame:int):void
		{
			mc.currentFrame =frame;
			mc.pause();
		}
		
		public static function gotoAndPlay(mc:*,frame:int):void
		{
			mc.currentFrame =frame;
			mc.play();
		}
		
		/*public static function getPartycleSystem(systemName:String,addToJugglerFlag:Boolean=true):PDParticleSystem
		{
		var system:PDParticleSystem = Assets.getParticleSystem(systemName);
		system.start();
		if (addToJugglerFlag)
		{
		Starling.juggler.add(system);
		}
		
		
		return system;
		}*/
		
		public static function getImage(imageName:String,atlasName:String = null,blendMode:String=null):Image
		{
			var img:Image;
			if (atlasName) {
				img = new Image(Assets.getTextureAtlas(atlasName).getTexture(imageName));
			}
			else{
				img =	new Image(Assets.getTexture(imageName));
			}
			
			img.smoothing = TextureSmoothing.BILINEAR; 
			
			if(blendMode)img.blendMode =BlendMode.NONE;
			
			return img;
			
		}
		
		public static function getButton(atlasName:String,upState:String,dwnState:String=null,text:String=""):*
		{
			var upTexture:Texture= upState ? Assets.getTextureAtlas(atlasName).getTexture(upState) : null;
			var dwnTexture:Texture= dwnState ? Assets.getTextureAtlas(atlasName).getTexture(dwnState) :null;
			
			var btn:starling.display.Button= new starling.display.Button(upTexture,text,dwnTexture);
			return btn;
			
		}
		
		
		
		public static function getTextField(width:Number=300,height:Number=50,text:String="",size:Number=50,font:String="museo",pivot:Boolean=false,color:uint=0xffffff,aspectRatio:Boolean=true):TextField
		{
			if(aspectRatio || isHD)
			{
				width = starHelper.getXPos(width);
				height = starHelper.getYPos(height);
				size = starHelper.getYPos(size);
			}
			
			var txtField:TextField = new TextField(width, height, text, font, size, color, true);
			txtField.autoScale = true;
			if(pivot)setPivot(txtField);
			return txtField;
		}
		
		
		public static function remove(mc:Object,nullFlag:Boolean=true):void
		{
			if(mc != null)
			{
				if (mc is MovieClip)
				{
					(mc as MovieClip).removeEventListeners();
					Starling.juggler.remove(mc as MovieClip);
					
				}
				
				if(mc.hasOwnProperty("remove"))
				{
					mc.remove();
				}
				
				if(mc.hasOwnProperty("kill"))
				{
					mc.kill=true;
				}
				
				if(mc.hasOwnProperty("destroy"))
				{
					mc.destroy();
				}
				
				
				if(mc.hasOwnProperty("removeEventListeners"))mc.removeEventListeners();
				if(mc.hasOwnProperty("removeFromParent"))mc.removeFromParent(true);
				
				mc = null;
			}
		}
		
		
		public static function setAspectScale(mc:*,aspectScale:*=null):void
		{
			if(Constants.ASPECT_RATIO == "16x9" || Constants.ASPECT_RATIO == "3x2")
			{
				setScale(mc,aspectScale);
			}
			
		}
		
		public static function getAspectScale(oriScale:*,scaleFactor:Number=0.9):Number
		{
			if(Constants.ASPECT_RATIO == "16x9" || Constants.ASPECT_RATIO == "3x2")
			{
				return scaleFactor*oriScale;
			}
			else
			{
				return oriScale;
			}
			
		}
		
		public static function setPivot(sprite:Object,pivotX:*=null,pivotY:*=null,addOffset:Boolean=false):void
		{
			if(pivotX != null && pivotY!=null)
			{
				sprite.pivotX = pivotX;
				sprite.pivotY = pivotY;
				
			}
			else
			{
				var pivotX = sprite.width / 2;
				var pivotY = sprite.height / 2;
				
				if(sprite.pivotX != pivotX)
				{
					sprite.pivotX= pivotX;
					if(addOffset)sprite.x += pivotX;
				}
				
				if(sprite.pivotY != pivotY)
				{
					sprite.pivotY= pivotY;
					if(addOffset)sprite.y += pivotY;
				}
				
				
			}
			
		}
		
		
		public static function setPos(mc:*,posX:*=null,posY:*=null,xOffset:Array=null,yOffset:Array=null,aspectRatio:Boolean=false,aspectScale:*=null):void
		{
			//both xOffset,yOffset array  contains offsets for [16x9,3x2]
			if(posX != null)
			{
				if(Constants.ASPECT_RATIO == "16x9" && xOffset && xOffset[0])
				{
					if(aspectRatio){
						mc.x =getXPos(posX+xOffset[0]);
					}
					else
					{
						mc.x =posX + xOffset[0];
					}
				}
				else if(Constants.ASPECT_RATIO == "3x2" && xOffset && xOffset[1])
				{
					if(aspectRatio){
						mc.x =getXPos(posX+xOffset[1]);
					}
					else
					{
						mc.x =posX + xOffset[1];
					}
				}
				else if(Constants.ASPECT_RATIO.indexOf("HD") != -1 && xOffset && xOffset[2])
				{
					//4x3HD
					if(aspectRatio || isHD)
					{
						mc.x =getXPos(posX+xOffset[2]);
					}
					else
					{
						mc.x =posX + xOffset[2];
					}
				}
				else
				{
					if(aspectRatio || isHD){
						mc.x =getXPos(posX);
					}
					else
					{
						mc.x =posX;
					}
				}
			}
			
			if(posY !=null)
			{
				if(Constants.ASPECT_RATIO == "16x9" && yOffset && yOffset[0])
				{
					if(aspectRatio){
						mc.y =getYPos(posY+yOffset[0]);
					}
					else
					{
						mc.y =posY + yOffset[0];
						
					}
				}
				else if(Constants.ASPECT_RATIO == "3x2" && yOffset && yOffset[1])
				{
					if(aspectRatio){
						mc.y =getYPos(posY+yOffset[1]);
					}
					else
					{
						mc.y =posY + yOffset[1];
						
					}
				}
				else if(Constants.ASPECT_RATIO.indexOf("HD") != -1 && yOffset && yOffset[2])
				{
					//4x3HD
					if(aspectRatio || isHD)
					{
						mc.y =getYPos(posY+yOffset[2]);
					}
					else
					{
						mc.y =posY + yOffset[2];
						
					}
				}
				else
				{
					if(aspectRatio || isHD){
						mc.y =getYPos(posY);
					}
					else
					{
						mc.y =posY;
						
					}
				}
			}
			
			
			if(aspectScale != null)
			{
				setAspectScale(mc,aspectScale);
			}
			
		}
		
		public static function getHDXPos(posX:Number):Number
		{
			if(isHD)
			{
				return  Number((posX / 1024) * Constants.DEVICE_WIDTH); 	
			}
			else
			{
				return posX;
			}
		}
		
		public static function getHDYPos(posY:Number):Number
		{
			if(isHD)
			{
				return  Number((posY / 768) * Constants.DEVICE_HEIGHT); 
			}
			else
			{
				return posY;
			}
		}
		
		public static function disableTouch():void
		{
			
			if(!touchQuad)
			{
				trace('ADD NEW QUAD');
				touchQuad = new Quad(Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT,0xFFE6E6);
				touchQuad.x = 0;
				touchQuad.y = 0;
				touchQuad.alpha = 0;
				starHelper.stageRef.addChild(touchQuad);
			}
		}
		
		public static function enableTouch():void
		{
			
			if(touchQuad)
			{
				trace('REMOVE NEW QUAD');
				starHelper.remove(touchQuad);
				touchQuad = null;
			}
		}
		
		public static function getXPos(posX:Number):Number
		{
			return  Number((posX / 1024) * Constants.DEVICE_WIDTH); 	
		}
		
		public static function getYPos(posY:Number):Number
		{
			return  Number((posY / 768) * Constants.DEVICE_HEIGHT); 	
		}
		
		public static function getRevXPos(posX:Number):Number
		{
			return  Number((posX * 1024) / Constants.DEVICE_WIDTH); 	
		}
		
		public static function getRevYPos(posY:Number):Number
		{
			return  Number((posY * 768) / Constants.DEVICE_HEIGHT); 	
		}
		
		public static function getScreenWidth(e:*=null):Number
		{
			var screenWidth:Number;
			switch(Constants.ASPECT_RATIO)
			{
				case "16x9":
				{
					screenWidth = 1136;
					break;
				}
					
				case "3x2":
				{
					screenWidth = 960;
					
					break;
				}
					
				case "4x3":
				{
					screenWidth = 1024;
					
					break;
				}
					
				case "4x3HD":
				{
					screenWidth = 2048;
					
					break;
				}
			}
			
			return screenWidth;
		}
		
		public static function  get isHD():Boolean
		{
			return Constants.ASPECT_RATIO.indexOf("HD") != -1;
		}
		
		public static function getScreenHeight(e:*=null):Number
		{
			var screenHeight:Number;
			switch(Constants.ASPECT_RATIO)
			{
				case "16x9":
				{
					screenHeight = 640;
					break;
				}
					
				case "3x2":
				{
					screenHeight = 640;
					
					break;
				}
					
				case "4x3":
				{
					screenHeight = 768;
					
					break;
				}
					
				case "4x3HD":
				{
					screenHeight = 1536;
					
					break;
				}
			}
			
			return screenHeight;
		}
		
		public static  function removeAll(mc:*):void 
		{
			if(mc)
			{
				while(mc.numChildren)
				{
					remove(mc.getChildAt(0));
				}
				
			}
		}
		
		
		
	}
	
}

