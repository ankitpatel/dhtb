﻿package tilegame.star
{
	import starling.events.Event;
	
	public class StarlingEvent extends Event{
	   
		public var customData:Object
		public function StarlingEvent(type:String,cvar:Object=null, bubbles:Boolean = false, cancelable:Boolean = false) {
			// constructor code
			super(type, bubbles,cvar);
			customData=cvar;
			
			
       }	
    	
	}
	
}
