﻿package tilegame.star
{
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.system.System;
	
	import tilegame.Constants;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;
	
	public class Assets
	{
		public static const LEVEL_ATLAS:String = "level1_sp";
		
		public static var assets:AssetManager;
		
		
		public static function init(callback:Function):void
		{
			assets = new AssetManager(); 
			assets.verbose=true;
			
			assets.enqueue(Constants.ROOT_PATH+"FG/assets/level1_sp.png");
			assets.enqueue(Constants.ROOT_PATH+"FG/assets/level1_sp.xml");
			
			assets.enqueue(Constants.ROOT_PATH+"FG/assets/museo.png");
			assets.enqueue(Constants.ROOT_PATH+"FG/assets/museo.fnt");
			
			assets.addEventListener(Event.TEXTURES_RESTORED,onTextureRestored);
			//addContext3DEvent();
			
			assets.loadQueue(function(ratio:Number):void
			{
				if (ratio == 1.0)
				{
					callback();
				}
			}
			);
		}
		
		public static function getSwf(name:String):MovieClip
		{
			var mc:MovieClip = assets.getByteArray(name) as MovieClip;
			
			return mc;
		}
		
		private static function addContext3DEvent(e:*=null):void
		{
			import flash.events.Event;
			Starling.current.stage3D.addEventListener(flash.events.Event.CONTEXT3D_CREATE, onContextCreated, false, 0, true);
			
		}
		
		private static function onContextCreated(e:*=null):void
		{
			trace("Assets.onContextCreated(e)");
			
		}
		
		private static function onTextureRestored(e:*=null):void
		{
			trace("Assets.onTextureRestored(e)");
			
		}
		
		
		public static function disposeAll(e:*=null):void
		{
			TextField.unregisterBitmapFont('museo');
			if(assets)
			{
				assets.purge();
				assets=null;
			}
			System.gc();
		}
		
		public static function getTexture(name:String):Texture
		{
			
			var texture:Texture = assets.getTexture(name);
			return texture
		}
		
		public static function getTextureAtlas(screenName:String):TextureAtlas
		{
			var textures:TextureAtlas = assets.getTextureAtlas(screenName);
			return textures;
		}
	}
}