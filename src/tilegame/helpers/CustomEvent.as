﻿package tilegame.helpers
{
	import flash.events.Event;
	
	public class CustomEvent extends Event{
	   
		public var customData:Object
		public function CustomEvent(type:String,cvar:Object=null, bubbles:Boolean = false, cancelable:Boolean = false) {
			// constructor code
			super(type, bubbles,cancelable);
			customData=cvar;
       }	
    	
	}
	
}
