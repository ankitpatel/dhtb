package tilegame.helpers
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.utils.Dictionary;
	
	//import platform.model.DBHelper;
	/**
	 * ...
	 * @author ankit
	 */
	public class SWFPreLoader extends MovieClip
	{
		static public const PRELOADING_COMPLETE:String = "preloadingComplete";
		static public const PROGRESS:String = "progress";
		private var _swfPathArr:Array;
		public static var swfDic:Dictionary= new Dictionary();
		private var _loadedSWFs:int=0;
		private var _swfClipsArr:Array = new Array();
		private var _swfTempClip:MovieClip;
		private var _swfLoader:Loader;
		private var _swfRequest:URLRequest;
		private var totPerc:Number = 0;
		private var prevPerc:Number = -1;
		
		public function SWFPreLoader(swfArrToLoad:Array) 
		{
			_swfPathArr = swfArrToLoad;
			_loadedSWFs = 0;
			startLoading();
		}
		
		
		public function startLoading():void 
		{
			//get all game data from database
			_swfLoader = new Loader();
			_swfRequest = new URLRequest();
			totPerc = 0;
			loadSWF(_swfPathArr[_loadedSWFs]);
			
		}
		 
		function loadSWF(path:String):void {
			trace("Loading Game>>",path);
			setupListeners(_swfLoader.contentLoaderInfo);
		   
			_swfRequest.url = "FG/levels/"+path + ".swf"; //for local swfs
			trace(_swfRequest.url);
			
			var ldrContext:LoaderContext;
			
			if(Security.sandboxType == Security.REMOTE){
				ldrContext = new LoaderContext(false, ApplicationDomain.currentDomain, SecurityDomain.currentDomain);
				ldrContext.allowCodeImport = true;
				_swfLoader.load(_swfRequest, ldrContext);
		
			}else {
				ldrContext = new LoaderContext(false, ApplicationDomain.currentDomain, null);
				ldrContext.allowCodeImport = true;
				_swfLoader.load(_swfRequest,ldrContext);
		
			}
			
		}
		 
		function setupListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, onSwfComplete);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, currentSwfProgress);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, loaderIOErrorHandler);
			
		}
		 
		private function loaderIOErrorHandler(e:IOErrorEvent):void 
		{
			trace("IO ERROR in Loadign game swfs ");
		}
		
		
		function currentSwfProgress(event:ProgressEvent):void {
			var _perc:int = (event.bytesLoaded / event.bytesTotal) * 100;
			var sendPerc:int;
			
			if (_perc == 100)
			{
				totPerc += 100;
				sendPerc = int((totPerc) / _swfPathArr.length);
			}
			else
			{
				sendPerc = int((totPerc + _perc) / _swfPathArr.length);
			}
			
			if (sendPerc > 100)
			{
				dispatchEvent(new CustomEvent(PROGRESS, 100));
		
			}
			else if (sendPerc != prevPerc)
			{
				dispatchEvent(new CustomEvent(PROGRESS, sendPerc));
			}
			else if(sendPerc <= prevPerc)
			{
				dispatchEvent(new CustomEvent(PROGRESS, prevPerc));
			}
			
			trace("perc>>",_swfPathArr[_loadedSWFs],sendPerc,_perc);
		}
		
		function onSwfComplete(event:Event):void {
			event.target.removeEventListener(Event.COMPLETE, onSwfComplete);
			event.target.removeEventListener(ProgressEvent.PROGRESS, currentSwfProgress);
		 
			_swfTempClip = event.target.content;
			swfDic[_swfPathArr[_loadedSWFs]] = _swfTempClip;
			_loadedSWFs++;
			
			if (_loadedSWFs < _swfPathArr.length) {
			
				loadSWF(_swfPathArr[_loadedSWFs]);
			} else {
				trace("onCOmplete PRELOADING>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				_swfLoader.unloadAndStop();
				_swfLoader = null;
				onCompletePreloading();
			}
		}
		 
		function onCompletePreloading():void {
			
			dispatchEvent(new Event(PRELOADING_COMPLETE));
		}
		

	}

}