package tilegame.helpers
{
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	
	import tilegame.Constants;

	public class FBHelper
	{
		
		public static function init():void{
			ExternalInterface.addCallback("myFlashcall",myFlashcall);
			Constants.stage.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		public static function myFlashcall(str:String):void{
			trace("myFlashcall: "+str);
		}
		
		public static function onClick(event:MouseEvent):void{
			if(ExternalInterface.available){
				trace("onClick");
				ExternalInterface.call("myFBcall");
			}
		}
	}
}