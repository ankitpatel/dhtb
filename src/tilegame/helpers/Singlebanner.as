﻿package tilegame.helpers
{
	// Ane Extension Imports
	
	import com.codealchemy.ane.admobane.AdMobEvent;
	import com.codealchemy.ane.admobane.AdMobManager;
	import com.codealchemy.ane.admobane.AdMobPosition;
	import com.codealchemy.ane.admobane.AdMobSize;
	
	// Flash Imports
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	/** 
	 * Singlebanner Class<br/>
	 * The class will construct The Single Banner Example
	 *
	 **/
	public class Singlebanner extends Sprite
	{
		private const BANNER_ID = "myBanner";
		private var adMobManager:AdMobManager;
		private var dispatcher:EventDispatcher;
		private var isShow:Boolean = false;
		
		// =================================================================================================
		//	Constructor / Deconstructors
		// =================================================================================================
		
		/** 
		 * Singlebanner Constructor
		 *
		 **/
		public function Singlebanner()
		{
			// Init Sprite
			super();
			
			// Get the AdMobManager instance
			adMobManager = AdMobManager.manager;
			// Check if the Extension is supported
			if(adMobManager.isSupported){
				// Get the dispatcher instance
				dispatcher = adMobManager.dispatcher;
				// Set Operation settings
				adMobManager.verbose = true;
				adMobManager.operationMode = AdMobManager.PROD_MODE;
				//adMobManager.testDeviceID = "390CF00F3C79AACF";
				
				// Set AdMobId settings
				adMobManager.bannersAdMobId = "ca-app-pub-8456996289248733/6688219609";
				
				// Add Event Listeners
				addBannersListeners();
				adMobManager.createBanner(AdMobSize.BANNER,AdMobPosition.BOTTOM_CENTER,BANNER_ID, null, true);
			}
			
			
		}
		
		
		/** 
		 * Add all the event Listeners for the banners
		 * 
		 */
		private function addBannersListeners():void
		{
			if(CONFIG::ADD)
			{
				// onBannerLoaded Event Listener
				if (!dispatcher.hasEventListener(AdMobEvent.BANNER_LOADED))
					dispatcher.addEventListener(AdMobEvent.BANNER_LOADED, onBannerLoaded);
				
				// onBannerFailedToLoad Event Listener
				if (!dispatcher.hasEventListener(AdMobEvent.BANNER_FAILED_TO_LOAD))
					dispatcher.addEventListener(AdMobEvent.BANNER_FAILED_TO_LOAD, onBannerFailedToLoad);
			}
		}
		
		private function removeBannersListeners():void
		{
			// onBannerLoaded Event Listener
			if (dispatcher.hasEventListener(AdMobEvent.BANNER_LOADED))
				dispatcher.removeEventListener(AdMobEvent.BANNER_LOADED, onBannerLoaded);
			
			// onBannerFailedToLoad Event Listener
			if (dispatcher.hasEventListener(AdMobEvent.BANNER_FAILED_TO_LOAD))
				dispatcher.removeEventListener(AdMobEvent.BANNER_FAILED_TO_LOAD, onBannerFailedToLoad);
			
		}
		
		public function onCreate(e:*=null):void
		{
			
			// Create the Banner
			if(adMobManager.isSupported) adMobManager.createBanner(AdMobSize.BANNER,AdMobPosition.MIDDLE_CENTER,BANNER_ID, null, true);
			// Update banner state
			isShow = false;
			
		}
		
		private function onShow(e:*=null):void
		{
			
			// Show the Banner
			if(adMobManager.isSupported) adMobManager.showBanner(BANNER_ID);
			// Update banner state
			isShow = true;
			
		}
		
		
		private function onHide(e:*=null):void
		{
			// Hide the Banner
			if(adMobManager.isSupported) adMobManager.hideBanner(BANNER_ID);
			// Update banner state
			isShow = false;
			
			
		}
		
		
		public function onDestroy(e:Event):void
		{
			// Destroy the Banner
			if(adMobManager.isSupported) adMobManager.removeBanner(BANNER_ID);
			
		}
		
		private function onBannerLoaded(e:AdMobEvent):void
		{
			trace("Singlebanner.onBannerLoaded(e)");
			
			// Update banner state
			isShow = true;
			
		}
		
		private function onBannerFailedToLoad(e:AdMobEvent):void
		{
			trace("Singlebanner.onBannerFailedToLoad(e)");
			
			// Update banner state
			isShow = false;
			
		}
	}
}