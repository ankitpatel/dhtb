package tilegame.helpers
{
	import com.freshplanet.ane.AirGooglePlayGames.AirGooglePlayGames;
	import com.freshplanet.ane.AirGooglePlayGames.AirGooglePlayGamesEvent;

	public class GSHelper
	{
		public static const NORMAL_LEADERBOARD_ID:String = "CgkI697-g5ccEAIQBg";
		public static const FASTER_LEADERBOARD_ID:String = "CgkI697-g5ccEAIQBw";
		public static const PRO_LEADERBOARD_ID:String = "CgkI697-g5ccEAIQCA";
		
		public static const ACHIV_NORMAL_50:String = "CgkI697-g5ccEAIQAQ";
		public static const ACHIV_NORMAL_100:String = "CgkI697-g5ccEAIQAg";
		public static const ACHIV_FASTER_30:String = "CgkI697-g5ccEAIQAw";
		public static const ACHIV_FASTER_50:String = "CgkI697-g5ccEAIQBA";
		public static const ACHIV_PRO_30:String = "CgkI697-g5ccEAIQBQ";
		
		public function GSHelper()
		{
			
		}
		
		public static function init(e:*=null):void
		{
			AirGooglePlayGames.getInstance().addEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_SUCCESS, onSignInSuccess);
			AirGooglePlayGames.getInstance().addEventListener(AirGooglePlayGamesEvent.ON_SIGN_OUT_SUCCESS, onSignOutSuccess);
			AirGooglePlayGames.getInstance().addEventListener(AirGooglePlayGamesEvent.ON_SIGN_IN_FAIL, onSignInFail);
			AirGooglePlayGames.getInstance().startAtLaunch();
		}
		
		public static function reportAchievement(achievementId:String,steps:*=null):void
		{
			if(steps)
			{
				AirGooglePlayGames.getInstance().reportAchievement(achievementId,steps);
			}
			else
			{
				AirGooglePlayGames.getInstance().reportAchievement(achievementId);
			}
		}
		
		public static function showAchievements(e:*=null):void
		{
			AirGooglePlayGames.getInstance().showStandardAchievements();
			
		}
		
		public static function reportScore(leaderbordId:String, value:*):void
		{
			AirGooglePlayGames.getInstance().reportScore(leaderbordId, value);
		}
		
		public static function onSignInSuccess(e:*=null):void
		{
			trace("GSHelper.onSignInSuccess(e)");
			
		}
		
		public static function onSignOutSuccess(e:*=null):void
		{
			trace("GSHelper.onSignOutSuccess(e)");
			
		}
		
		public static function onSignInFail(e:*=null):void
		{
			trace("GSHelper.onSignInFail(e)");
			
		}
	}
}