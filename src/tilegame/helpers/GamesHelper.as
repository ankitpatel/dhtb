package tilegame.helpers
{
	import com.google.api.games.Games;
	import com.google.api.games.SignIn;
	
	import flash.events.StatusEvent;

	public class GamesHelper
	{
		public function GamesHelper()
		{
		}
		
		public static function init(e:*=null):void
		{
			Games.initialize();
			// some config commands, for example, Games.promptUserToSignInOnStartup()
			Games.start();
			
		}
		
		public static function signIN(e:*=null):void
		{
			
			//if(Games.isSignedIn() == false)
			{
				Games.beginUserInitiatedSignIn();
				//Games.promptUserToSignInOnStartup(true);
				Games.addStatusEventListener(onStatusEventListener);
			}
		}
		
		
		private static function onStatusEventListener(e:StatusEvent):void
		{
			trace("GamesHelper.onStatusEventListener(e)");
			
			switch (e.code)
			{
				case SignIn.SUCCESS: 
					trace('success sign in');
					break;
				case SignIn.FAIL: 
					trace('fail sign in');
					
					break;
			}
		}
	}
}