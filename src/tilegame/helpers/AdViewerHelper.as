﻿package tilegame.helpers
{
	import com.adobe.promotion.AdViewer;
	import com.adobe.promotion.AdViewerEvent;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.errors.IllegalOperationError;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import tilegame.Constants;
	
	public class AdViewerHelper extends Sprite
	{
		
		public var  logger:TextField = new TextField();
		public var adViewer:AdViewer = null ; 
		public static const APPLICATION_ID:String = "Dhtba18";
		
		public function AdViewerHelper()
		{
			super();
			
			init();
		}
		
		
		protected function init():void
		{
			
			trace("AdViewerHelper.init()");
			
			// for testing purpose, create a  AdViewer class with default value  
			adViewer= AdViewer.getAdViewer(Constants.stage,APPLICATION_ID);
			
			if(adViewer)
			{
				// for production, create a AdViewer class with Adobe provided applicationId 
				// var applicationId:String = "100001";
				//adViewer= AdViewer.getAdViewer(this.stage,applicationId);
				
				
				//adding event handler when add is loaded completely 
				adViewer.addEventListener(Event.COMPLETE,onComplete);
				
				// adding event handler for  when user has clicked or closed the ad
				adViewer.addEventListener(Event.CLOSE,onUserInteraction);
				adViewer.addEventListener(AdViewerEvent.AD_CLICK,onUserInteraction);
				
				// Event when error 
				adViewer.addEventListener(ErrorEvent.ERROR,onError);
			}
			
		}
		
		protected function onError(event:*=null):void
		{
			logger.text="Ad failed to load : \n"+logger.text;
			
		}
		
		protected function onUserInteraction(event:*=null):void
		{
			
			if (event.type=="adClick"){
				logger.text="Ad is clicked :loading next ad \n"+logger.text;
			}else if (event.type=="close"){
				logger.text="Ad is closed  :loading next ad \n"+logger.text;
				//adViewer.load();
				
			}
			
		}
		
		protected function onComplete(event:*=null):void
		{
			trace("AdViewerHelper.onComplete(event)");
			
			logger.text="Ad is loaded : \n"+logger.text;
			adViewer.show();//
		}
		
		public function requestShowAd(event:*=null):void
		{
			
			try {
				adViewer.show();
			}
			catch(error:IllegalOperationError)
			{
				
				logger.text="IllegalOperationError ErrorID: " + error.errorID + "\n Error message: " + error.message + "\n"+logger.text;
			}
			catch (error:Error)
			{
				
				logger.text="ErrorID: " + error.errorID + "\n Error message: " + error.message +"\n"+logger.text;
			}
			
		}
		
		protected function requestloadAd(event:*=null):void
		{
			try {
				
				
				logger.text="Requesting Ad: \n"+ logger.text;
				
				adViewer.load();
			}
			catch(error:IllegalOperationError)
			{
				
				logger.text="IllegalOperationError ErrorID: " + error.errorID + "\n Error message: " + error.message + "\n"+logger.text;
			}
			catch (error:Error)
			{
				
				logger.text="ErrorID: " + error.errorID + "\n Error message: " + error.message +"\n"+logger.text;
			}
			
		}
	}
}