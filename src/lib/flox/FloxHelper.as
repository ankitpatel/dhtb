﻿package lib.flox
{
	import com.gamua.flox.Entity;
	import com.gamua.flox.Flox;
	import com.gamua.flox.Player;
	import com.gamua.flox.Query;
	import com.playpower.logger.Mixpanel;
	
	import flash.events.Event;
	import flash.events.UncaughtErrorEvent;
	import flash.utils.getTimer;
	
	import lib.flox.entities.FloxPlayerVO;
	
	import tilegame.Constants;
	
	
	public class FloxHelper
	{
		private static var isInitialized:Boolean=false;
		private static var currentPlayer:Player;
		
		public function FloxHelper()
		{
		}
		
		public static function init():void
		{
			
			//start flox
			if(!isInitialized)
			{
				Flox.init('LpRf2Dmdkp9QIsR2', 'kvSnel9OZ3YZDnLT',Constants.CURRENT_VERSION);
				
				Constants.stageRef.loaderInfo.uncaughtErrorEvents.addEventListener(
					UncaughtErrorEvent.UNCAUGHT_ERROR, function(event:UncaughtErrorEvent):void {
						Flox.logError(event.error, "Uncaught Error: " + event.error.message);
					}
				);
				
				
				isInitialized=true;
				
				
			}
			
		}
		
		
		public static function login(_id:String):void
		{
			Player.loginWithKey(_id,
				function onComplete(player:Player) {
					//Yay! The player is logged in!
					currentPlayer = Player.current;
					trace("LOGGED IN PLAYER _id,_floxPLayerID>>",_id,playerID)
				},
				function onError(message:String) {
					trace("! Something went wrong while authenticating the player.")
				}
			);
		}
		
		public static function logout():void
		{
			Player.logout();
		}
		
		public static function playerID():String
		{
			//returns current player flox unique id
			return currentPlayer.authId;
		}
		
		
		public static function logEvent(event:String,object:Object=null,mixPanelLog:Boolean=true):void
		{
			trace("FLOX EVENT LOOGED","[",event,"]",JSON.stringify(object));
			
			Flox.logEvent(event,object);
			
		}
		
		
		
		public static function logError(event:*,object:*=null,flurryLog:Boolean=true):void
		{
			Flox.logError(event,object);
		}
		
		public static function saveEntity(entityObj:*):void
		{
			//save it to flox using the instance method
			entityObj.saveQueued();
		}
		
		public static function getEntity(entityClass:*,entityID:String,callBack:Function):void
		{
			trace("FloxHelper.getEntity(entityClass, entityID, callBack)");
			
			//load the associated entity of type Savegame
			Entity.load(entityClass, entityID,
				function onComplete(entityObj:*):void {
					//Everything worked just fine and an entity has been retrieved from the server.
					callBack(entityObj);
				},
				function onError(error:String, httpStatus:*):void {
					trace("prevSocre was>>>>onError");
					callBack();
				}
			);
		}
		
		public static function deleteEntity(entityClass:*,entityID:String):void
		{
			Entity.destroyQueued(entityClass, entityID);
		}
		
		
		
		public static function getQeueryResults(entityClass:*,whereClause:String=null,args:Array=null,_limit:Number=50,_offset:Number=0,callback:Function=null):void
		{
			//let's create a new query object and tell it to query on entities of type 'Track'
			var query:Query = new Query(entityClass,whereClause,args);
			query.limit =_limit;
			query.offset =_offset;
			
			//without further ado, let's load all the tracks the current player can see
			query.find(
				function onComplete(entities:Array):void {
					//The tracks array contains all tracks the current player
					if(callback)callback(entities)
				},
				function onError(error:String):void {
					//Something went wrong during the execution of the query.
					//The player's device may be offline.
					trace("ERROR IN FLOX QUERY");
				}
			);
		}
	}
}