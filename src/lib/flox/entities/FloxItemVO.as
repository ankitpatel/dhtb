package lib.flox.entities
{
	import com.gamua.flox.Entity;
	
	public class FloxItemVO extends Entity
	{
		private var _pfid:String;
		private var _lid:String;
		private var _playerid:String;
		private var _sessionid:String;
		private var _deviceid:String;
		private var _game:String;
		private var _iid:String;
		private var _grade:String;
		private var _planet:String;
		private var _position:String;
		private var _level_position:String;
		private var _easy:String = '0';
		private var _attemptnumber:String = '1';
		private var _timestamp:String;
		private var _correct:String;
		private var _noofattempts:String;
		private var _reactiontime:String;
		
		private var _toid1:String='';
		private var _toid2:String='';
		
		public function FloxItemVO() {
		}
		
		public function get position():String
		{
			return _position;
		}

		public function set position(value:String):void
		{
			_position = value;
		}

		public function get level_position():String
		{
			return _level_position;
		}

		public function set level_position(value:String):void
		{
			_level_position = value;
		}

		public function get toid2():String
		{
			return _toid2;
		}

		public function set toid2(value:String):void
		{
			_toid2 = value;
		}

		public function get toid1():String
		{
			return _toid1;
		}

		public function set toid1(value:String):void
		{
			_toid1 = value;
		}

		public function get reactiontime():String
		{
			return _reactiontime;
		}

		public function set reactiontime(value:String):void
		{
			_reactiontime = value;
		}

		public function get noofattempts():String
		{
			return _noofattempts;
		}

		public function set noofattempts(value:String):void
		{
			_noofattempts = value;
		}

		public function get correct():String
		{
			return _correct;
		}

		public function set correct(value:String):void
		{
			_correct = value;
		}

		public function get timestamp():String
		{
			return _timestamp;
		}

		public function set timestamp(value:String):void
		{
			_timestamp = value;
		}

		public function get attemptnumber():String
		{
			return _attemptnumber;
		}

		public function set attemptnumber(value:String):void
		{
			_attemptnumber = value;
		}

		public function get easy():String
		{
			return _easy;
		}

		public function set easy(value:String):void
		{
			_easy = value;
		}

		

		public function get planet():String
		{
			return _planet;
		}

		public function set planet(value:String):void
		{
			_planet = value;
		}

		public function get grade():String
		{
			return _grade;
		}

		public function set grade(value:String):void
		{
			_grade = value;
		}

		public function get iid():String
		{
			return _iid;
		}

		public function set iid(value:String):void
		{
			_iid = value;
		}

		public function get game():String
		{
			return _game;
		}

		public function set game(value:String):void
		{
			_game = value;
		}

		
		public function get deviceid():String
		{
			return _deviceid;
		}

		public function set deviceid(value:String):void
		{
			_deviceid = value;
		}

		public function get sessionid():String
		{
			return _sessionid;
		}

		public function set sessionid(value:String):void
		{
			_sessionid = value;
		}

		public function get playerid():String
		{
			return _playerid;
		}

		public function set playerid(value:String):void
		{
			_playerid = value;
		}

		public function get lid():String
		{
			return _lid;
		}

		public function set lid(value:String):void
		{
			_lid = value;
		}

		public function get pfid():String
		{
			return _pfid;
		}

		public function set pfid(value:String):void
		{
			_pfid = value;
		}

	}
	
}