package lib.flox.entities
{
	import com.gamua.flox.Entity;
	
	public class FloxPushVO extends Entity
	{
		private var _deviceid:String;
		private var _payload:String;
		private var _open_method:String;
		
		public function FloxPushVO() {
		}
		
		
		public function get open_method():String
		{
			return _open_method;
		}

		public function set open_method(value:String):void
		{
			_open_method = value;
		}

		public function get payload():String
		{
			return _payload;
		}

		public function set payload(value:String):void
		{
			_payload = value;
		}

		
		public function get deviceid():String
		{
			return _deviceid;
		}

		public function set deviceid(value:String):void
		{
			_deviceid = value;
		}

	}
	
}