﻿package lib.flox.entities
{
	import com.gamua.flox.Access;
	import com.gamua.flox.Entity;
	import com.gamua.flox.Player;
	
	public class FloxPlayerVO extends Entity
	{
		private var _pfid:String;
		private var _pnid:String;
		private var _playerid:String;
		private var _player:String;
		private var _gender:String;
		private var _signupgrade:String;
		private var _curgrade:String;
		private var _times_opened:String='0';
		private var _last_logged_time:String;
		private var _user_creation_timestamp:String;
		private var _first_launch_timestamp:String;
		private var _sessionid:String;
		private var _deviceid:String;
		private var _total_time:String='0';
		private var _longest_session_time:String='0';
		private var _device:String='';
		private var _planet:String;
		private var _level_position:String;
		
		private var _percent_correct:String='0';
		private var _total_question:String='0';
		private var _total_correct:String='0';
		
		
		/////////inapp vars/////////
		private var _inapp_grade1:String='0';
		private var _inapp_grade1_position:String='';
		private var _inapp_grade1_price:String='';
		private var _inapp_grade1_timestamp:String='';
		
		private var _inapp_grade2:String='0';
		private var _inapp_grade2_position:String='';
		private var _inapp_grade2_price:String='';
		private var _inapp_grade2_timestamp:String='';
		
		private var _inapp_grade3:String='0';
		private var _inapp_grade3_position:String='';
		private var _inapp_grade3_price:String='';
		private var _inapp_grade3_timestamp:String='';
		
		private var _inapp_grade4:String='0';
		private var _inapp_grade4_position:String='';
		private var _inapp_grade4_price:String='';
		private var _inapp_grade4_timestamp:String='';
		
		private var _inapp_grade5:String='0';
		private var _inapp_grade5_position:String='';
		private var _inapp_grade5_price:String='';
		private var _inapp_grade5_timestamp:String='';
		
		private var _inapp_ms:String='0';
		private var _inapp_ms_position:String='';
		private var _inapp_ms_price:String='';
		private var _inapp_ms_timestamp:String='';
		
		private var _inapp_allgrades:String='0';
		private var _inapp_allgrades_position:String='';
		private var _inapp_allgrades_price:String='';
		private var _inapp_allgrades_timestamp:String='';
		
		
		
		public function FloxPlayerVO() {
			this.publicAccess = Access.READ_WRITE;
		}
		
		public function get total_correct():String
		{
			return _total_correct;
		}

		public function set total_correct(value:String):void
		{
			_total_correct = value;
		}

		public function get total_question():String
		{
			return _total_question;
		}

		public function set total_question(value:String):void
		{
			_total_question = value;
		}

		public function get percent_correct():String
		{
			return _percent_correct;
		}

		public function set percent_correct(value:String):void
		{
			_percent_correct = value;
		}

		public function get level_position():String
		{
			return _level_position;
		}

		public function set level_position(value:String):void
		{
			_level_position = value;
		}

		public function get planet():String
		{
			return _planet;
		}

		public function set planet(value:String):void
		{
			_planet = value;
		}

		public function get device():String
		{
			return _device;
		}

		public function set device(value:String):void
		{
			_device = value;
		}

		public function get total_time():String
		{
			return _total_time;
		}

		public function set total_time(value:String):void
		{
			_total_time = value;
		}

		public function get longest_session_time():String
		{
			return _longest_session_time;
		}

		public function set longest_session_time(value:String):void
		{
			_longest_session_time = value;
		}

		public function get pnid():String
		{
			return _pnid;
		}

		public function set pnid(value:String):void
		{
			_pnid = value;
		}

		public function get inapp_allgrades_timestamp():String
		{
			return _inapp_allgrades_timestamp;
		}

		public function set inapp_allgrades_timestamp(value:String):void
		{
			_inapp_allgrades_timestamp = value;
		}

		public function get inapp_allgrades_price():String
		{
			return _inapp_allgrades_price;
		}

		public function set inapp_allgrades_price(value:String):void
		{
			_inapp_allgrades_price = value;
		}

		public function get inapp_allgrades_position():String
		{
			return _inapp_allgrades_position;
		}

		public function set inapp_allgrades_position(value:String):void
		{
			_inapp_allgrades_position = value;
		}

		public function get inapp_ms_timestamp():String
		{
			return _inapp_ms_timestamp;
		}

		public function set inapp_ms_timestamp(value:String):void
		{
			_inapp_ms_timestamp = value;
		}

		public function get inapp_ms_price():String
		{
			return _inapp_ms_price;
		}

		public function set inapp_ms_price(value:String):void
		{
			_inapp_ms_price = value;
		}

		public function get inapp_ms_position():String
		{
			return _inapp_ms_position;
		}

		public function set inapp_ms_position(value:String):void
		{
			_inapp_ms_position = value;
		}

		public function get inapp_grade5_timestamp():String
		{
			return _inapp_grade5_timestamp;
		}

		public function set inapp_grade5_timestamp(value:String):void
		{
			_inapp_grade5_timestamp = value;
		}

		public function get inapp_grade5_price():String
		{
			return _inapp_grade5_price;
		}

		public function set inapp_grade5_price(value:String):void
		{
			_inapp_grade5_price = value;
		}

		public function get inapp_grade5_position():String
		{
			return _inapp_grade5_position;
		}

		public function set inapp_grade5_position(value:String):void
		{
			_inapp_grade5_position = value;
		}

		public function get inapp_grade4_timestamp():String
		{
			return _inapp_grade4_timestamp;
		}

		public function set inapp_grade4_timestamp(value:String):void
		{
			_inapp_grade4_timestamp = value;
		}

		public function get inapp_grade4_price():String
		{
			return _inapp_grade4_price;
		}

		public function set inapp_grade4_price(value:String):void
		{
			_inapp_grade4_price = value;
		}

		public function get inapp_grade4_position():String
		{
			return _inapp_grade4_position;
		}

		public function set inapp_grade4_position(value:String):void
		{
			_inapp_grade4_position = value;
		}

		public function get inapp_grade3_timestamp():String
		{
			return _inapp_grade3_timestamp;
		}

		public function set inapp_grade3_timestamp(value:String):void
		{
			_inapp_grade3_timestamp = value;
		}

		public function get inapp_grade3_price():String
		{
			return _inapp_grade3_price;
		}

		public function set inapp_grade3_price(value:String):void
		{
			_inapp_grade3_price = value;
		}

		public function get inapp_grade3_position():String
		{
			return _inapp_grade3_position;
		}

		public function set inapp_grade3_position(value:String):void
		{
			_inapp_grade3_position = value;
		}

		public function get inapp_grade2_timestamp():String
		{
			return _inapp_grade2_timestamp;
		}

		public function set inapp_grade2_timestamp(value:String):void
		{
			_inapp_grade2_timestamp = value;
		}

		public function get inapp_grade2_price():String
		{
			return _inapp_grade2_price;
		}

		public function set inapp_grade2_price(value:String):void
		{
			_inapp_grade2_price = value;
		}

		public function get inapp_grade2_position():String
		{
			return _inapp_grade2_position;
		}

		public function set inapp_grade2_position(value:String):void
		{
			_inapp_grade2_position = value;
		}

		public function get inapp_grade1_timestamp():String
		{
			return _inapp_grade1_timestamp;
		}

		public function set inapp_grade1_timestamp(value:String):void
		{
			_inapp_grade1_timestamp = value;
		}

		public function get inapp_grade1_price():String
		{
			return _inapp_grade1_price;
		}

		public function set inapp_grade1_price(value:String):void
		{
			_inapp_grade1_price = value;
		}

		public function get inapp_grade1_position():String
		{
			return _inapp_grade1_position;
		}

		public function set inapp_grade1_position(value:String):void
		{
			_inapp_grade1_position = value;
		}

		public function get curgrade():String
		{
			return _curgrade;
		}
		
		public function set curgrade(value:String):void
		{
			_curgrade = value;
		}
		
		public function get inapp_ms():String
		{
			return _inapp_ms;
		}
		
		public function set inapp_ms(value:String):void
		{
			_inapp_ms = value;
		}
		
		public function get inapp_allgrades():String
		{
			return _inapp_allgrades;
		}
		
		public function set inapp_allgrades(value:String):void
		{
			_inapp_allgrades = value;
		}
		
		public function get inapp_grade5():String
		{
			return _inapp_grade5;
		}
		
		public function set inapp_grade5(value:String):void
		{
			_inapp_grade5 = value;
		}
		
		public function get inapp_grade4():String
		{
			return _inapp_grade4;
		}
		
		public function set inapp_grade4(value:String):void
		{
			_inapp_grade4 = value;
		}
		
		public function get inapp_grade3():String
		{
			return _inapp_grade3;
		}
		
		public function set inapp_grade3(value:String):void
		{
			_inapp_grade3 = value;
		}
		
		public function get inapp_grade2():String
		{
			return _inapp_grade2;
		}
		
		public function set inapp_grade2(value:String):void
		{
			_inapp_grade2 = value;
		}
		
		public function get inapp_grade1():String
		{
			return _inapp_grade1;
		}
		
		public function set inapp_grade1(value:String):void
		{
			_inapp_grade1 = value;
		}
		
		public function get first_launch_timestamp():String
		{
			return _first_launch_timestamp;
		}
		
		public function set first_launch_timestamp(value:String):void
		{
			_first_launch_timestamp = value;
		}
		
		public function get user_creation_timestamp():String
		{
			return _user_creation_timestamp;
		}
		
		public function set user_creation_timestamp(value:String):void
		{
			_user_creation_timestamp = value;
		}
		
		public function get last_logged_time():String
		{
			return _last_logged_time;
		}
		
		public function set last_logged_time(value:String):void
		{
			_last_logged_time = value;
		}
		
		public function get times_opened():String
		{
			return _times_opened;
		}
		
		public function set times_opened(value:String):void
		{
			_times_opened = value;
		}
		
		
		
		public function get signupgrade():String
		{
			return _signupgrade;
		}
		
		public function set signupgrade(value:String):void
		{
			_signupgrade = value;
		}
		
		public function get gender():String
		{
			return _gender;
		}
		
		public function set gender(value:String):void
		{
			_gender = value;
		}
		
		public function get player():String
		{
			return _player;
		}
		
		public function set player(value:String):void
		{
			_player = value;
		}
		
		
		public function get deviceid():String
		{
			return _deviceid;
		}
		
		public function set deviceid(value:String):void
		{
			_deviceid = value;
		}
		
		public function get sessionid():String
		{
			return _sessionid;
		}
		
		public function set sessionid(value:String):void
		{
			_sessionid = value;
		}
		
		public function get playerid():String
		{
			return _playerid;
		}
		
		public function set playerid(value:String):void
		{
			_playerid = value;
		}
		
		
		public function get pfid():String
		{
			return _pfid;
		}
		
		public function set pfid(value:String):void
		{
			_pfid = value;
		}
		
		
	};
	
}
	
