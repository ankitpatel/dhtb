﻿package lib.flox.entities
{
	import com.gamua.flox.Entity;
	
	public class FloxLevelVO extends Entity
	{
		private var _pfid:String;
		private var _pnid:String;
		private var _lid:String;
		private var _playerid:String;
		private var _sessionid:String;
		private var _deviceid:String;
		private var _game:String;
		private var _grade:String;
		private var _planet:String;
		private var _level_position:String;
		private var _easy:String = '0';
		private var _attemptnumber:String = '1';
		private var _timestamp:String;
		private var _coins:String;
		private var _stars:String;
		private var _total_time:String;
		private var _avg_react_time:String;
		private var _percent_correct:String;
		private var _n_items:String;
		private var _toid1:String='';
		private var _toid2:String='';
		private var _complete:String='0';
		
		
		public function FloxLevelVO() {
		}
		
		public function get complete():String
		{
			return _complete;
		}

		public function set complete(value:String):void
		{
			_complete = value;
		}

		public function getObject():Object
		{
			var obj:Object= new Object();
			obj.pfid= pfid;
			obj.pnid= pnid;
			obj.lid= lid;
			obj.playerid= playerid;
			obj.sessionid= sessionid;
			obj.deviceid= deviceid;
			obj.game= game;
			obj.grade= grade;
			obj.planet= planet;
			obj.level_position= level_position;
			obj.easy= easy;
			obj.attemptnumber= attemptnumber;
			obj.timestamp= timestamp;
			obj.coins= coins;
			obj.stars= stars;
			obj.total_time= total_time;
			obj.toid1= toid1;
			obj.toid2= toid2;
			obj.avg_react_time= avg_react_time;
			obj.percent_correct= percent_correct;
			obj.n_items= n_items;
			obj.complete= complete;
			
			return obj;
			
		}
		
		public function get level_position():String
		{
			return _level_position;
		}

		public function set level_position(value:String):void
		{
			_level_position = value;
		}

		public function get toid2():String
		{
			return _toid2;
		}

		public function set toid2(value:String):void
		{
			_toid2 = value;
		}

		public function get toid1():String
		{
			return _toid1;
		}

		public function set toid1(value:String):void
		{
			_toid1 = value;
		}

		public function get n_items():String
		{
			return _n_items;
		}

		public function set n_items(value:String):void
		{
			_n_items = value;
		}

		public function get pnid():String
		{
			return _pnid;
		}

		public function set pnid(value:String):void
		{
			_pnid = value;
		}

		public function get percent_correct():String
		{
			return _percent_correct;
		}

		public function set percent_correct(value:String):void
		{
			_percent_correct = value;
		}

		public function get avg_react_time():String
		{
			return _avg_react_time;
		}

		public function set avg_react_time(value:String):void
		{
			_avg_react_time = value;
		}

		public function get total_time():String
		{
			return _total_time;
		}

		public function set total_time(value:String):void
		{
			_total_time = value;
		}

		public function get stars():String
		{
			return _stars;
		}

		public function set stars(value:String):void
		{
			_stars = value;
		}

		public function get coins():String
		{
			return _coins;
		}

		public function set coins(value:String):void
		{
			_coins = value;
		}

		public function get timestamp():String
		{
			return _timestamp;
		}

		public function set timestamp(value:String):void
		{
			_timestamp = value;
		}

		public function get attemptnumber():String
		{
			return _attemptnumber;
		}

		public function set attemptnumber(value:String):void
		{
			_attemptnumber = value;
		}

		public function get easy():String
		{
			return _easy;
		}

		public function set easy(value:String):void
		{
			_easy = value;
		}

		
		public function get planet():String
		{
			return _planet;
		}

		public function set planet(value:String):void
		{
			_planet = value;
		}

		public function get grade():String
		{
			return _grade;
		}

		public function set grade(value:String):void
		{
			_grade = value;
		}

		public function get game():String
		{
			return _game;
		}

		public function set game(value:String):void
		{
			_game = value;
		}

		
		public function get deviceid():String
		{
			return _deviceid;
		}

		public function set deviceid(value:String):void
		{
			_deviceid = value;
		}

		public function get sessionid():String
		{
			return _sessionid;
		}

		public function set sessionid(value:String):void
		{
			_sessionid = value;
		}

		public function get playerid():String
		{
			return _playerid;
		}

		public function set playerid(value:String):void
		{
			_playerid = value;
		}

		public function get lid():String
		{
			return _lid;
		}

		public function set lid(value:String):void
		{
			_lid = value;
		}

		public function get pfid():String
		{
			return _pfid;
		}

		public function set pfid(value:String):void
		{
			_pfid = value;
		}

	}
	
}