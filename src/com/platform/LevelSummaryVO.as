﻿package com.platform
{
	/**
	 * ...
	 * @author ankit
	 */
	public class LevelSummaryVO 
	{
		public var microno:String;
		public var stars:Number=0;
		public var coins:Number=0;
		public var totalTime:Number=0;
		public var totalQues:Number=0;
		public var totalCorrect:Number=0;
		
		/*All items data*/
		public var itemData:Vector.<PlatformItemVO>;
		
		/*All micro levels data */
		public var microData:Vector.<PlatformMicroVO>;
		
		public function LevelSummaryVO() 
		{
			
		}
		
		


	}

}