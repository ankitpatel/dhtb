﻿package com.platform
{
	/**
	 * ...
	 * @author ankit
	 */
	public class PlatformItemVO 
	{
		/*unique item/question id*/
		public var itemID:String; 
		
		/*topic id*/
		public var toid:String; 
		
		/*1 -correct or 0 -incorrect - assign 1 if one answers on 1st attempt else 0*/
		public var isCorret:int=0;   
		
		/*no of attemps of single item*/
		public var noOfAttempts:int=0; 
		
		/*item reaction time in seconds*/
		public var reactionTime:Number=0; 
		
		
		public var timestamp:String; 
		
		public function PlatformItemVO() 
		{
			var now:Date = new Date();
			var utc = now.getTime() + (now.getTimezoneOffset() * 60000);
			timestamp = String(utc);
		}
		

	}
	
}