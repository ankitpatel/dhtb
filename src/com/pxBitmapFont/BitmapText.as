﻿package com.pxBitmapFont
{
	import com.developmentarc.core.datastructures.utils.HashTable;
	import com.greensock.plugins.VisiblePlugin;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.text.TextField;
	
	import mathplanet.helpers.BPHelper;
	import mathplanet.star.AssetEmbeds;
	
	public class BitmapText extends MovieClip
	{
		private static var fontDic:HashTable= new HashTable();
		static private var textureClass:Class;
		
		public function BitmapText()
		{
		}
		
		private static function init(e:*=null):void
		{
			addBitmapFont("dimbo");
		}
		
		private static function addBitmapFont(font:String):void
		{
			if(!fontDic.containsKey(font))
			{
				var png:String = font + "Texture";
				var xmlName:String = font + "Xml";
				var bmfont:PxBitmapFont = new PxBitmapFont().loadAngelCode(getBitmapData(png), XML(create(xmlName)));
				
				fontDic.addItem(font,bmfont);
			}
		}
		
		private static function getBitmapData(name:String):BitmapData
		{
			trace("BitmapText.create(name)");
			var data:* = create(name);
			return data.bitmapData;
			
		}
		
		private static function create(name:String):Object
		{
			textureClass = AssetEmbeds;
			return new textureClass[name]();
			
		}
		
		public static function getTextField(font:String,text:String,color:uint=0xffffff,fontScale:Number=1,multiline:Boolean=false,wordwrap:Boolean=false):PxTextField
		{
			if(!fontDic.containsKey(font))
			{
				addBitmapFont(font);
			}
			
			var bitmapFont:PxBitmapFont= fontDic.getItem(font);
			
			var tf2:PxTextField = new PxTextField(bitmapFont);
			tf2.useColor = false;
			tf2.alignment = PxTextAlign.CENTER;
			//tf2.backgroundColor = color;
			//tf2.background=false;
			//tf2.lineSpacing = 0;
			tf2.fontScale = fontScale;
			//tf2.padding = 0;
			//tf2.letterSpacing = 5;
			//tf2.autoUpperCase = false;
			tf2.multiLine = multiline;
			tf2.wordWrap = wordwrap;
			tf2.width = 245;
			tf2.fixedWidth=true;
			tf2.fixedWidth = false;
			tf2.text = text;
			tf2.visible=true;
			
			
			return tf2;
		}
		
		public static function replaceTextField(txtField:TextField,font:String):PxTextField
		{
			var txt:PxTextField= getTextField(font,txtField.text);
			txt.x= txtField.x;
			txt.y= txtField.y;
			txtField.parent.addChild(txt);
			//BPHelper.remove(txtField);
			return txt;
		}
	}
}