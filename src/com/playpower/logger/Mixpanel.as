package com.playpower.logger
{
	import com.playpower.logger.utils.Base64;
	import com.playpower.logger.utils.HttpMethod;
	
	import flash.events.EventDispatcher;
	
	public class Mixpanel extends EventDispatcher
	{
		public static var service:RestService;
		public static var token:String;
		public static var apiKey:String;
		
		public static function logEvent(eventName:String, playerID:String, eventProperties:Object):void
		{
			var dt:Date = new Date();
			
			eventProperties["token"] = token;
			eventProperties["distinct_id"] = playerID;
			eventProperties["time"] = Math.round(dt.time / 1000);
			
			var finalStr:String = JSON.stringify({"event": eventName, "properties": eventProperties});
			
			finalStr = Base64.encode(finalStr);
			
			trace(finalStr);
			
			service.requestQueued(HttpMethod.GET, "http://api.mixpanel.com/import/", {"data": finalStr, "api_key": apiKey});
		}
		
		public static function initialize(mToken:String, mApiKey:String, gameID:String = "DefaultGameID"):void
		{
			token = mToken;
			apiKey = mApiKey;
			service = new RestService(gameID);
		}
		
		public static function clear():void
		{
			if (service)
			{
				service.clearCache();
				service.clearQueue();
			}
		}
	}
}