﻿// =================================================================================================
//
//	Flox AS3
//	Copyright 2012 Gamua OG. All Rights Reserved.
//
// =================================================================================================

package com.playpower.logger
{
    import com.playpower.logger.events.QueueEvent;
    import com.playpower.logger.utils.Base64;
    import com.playpower.logger.utils.DateUtil;
    import com.playpower.logger.utils.HttpMethod;
    import com.playpower.logger.utils.HttpStatus;
    import com.playpower.logger.utils.cloneObject;
    import com.playpower.logger.utils.createURL;
    import com.playpower.logger.utils.execute;
    import com.playpower.logger.utils.setTimeout;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.utils.ByteArray;

    /** A class that makes it easy to communicate with the Flox server via a REST protocol. */
    internal class RestService extends EventDispatcher
    {
        private var mUrl:String;
        private var mQueue:PersistentQueue;
        private var mCache:PersistentStore;
        private var mAlwaysFail:Boolean;
        private var mProcessingQueue:Boolean;
        
        /** Helper objects */
        private static var sBuffer:ByteArray = new ByteArray();
        
        /** Create an instance with the base URL of the Flox service. The class will allow 
         *  communication with the entities of a certain game (identified by id and key). */
        public function RestService(gameID:String)
        {
            mAlwaysFail = false;
            mProcessingQueue = false;
            mQueue = new PersistentQueue("Logger.RestService.queue." + gameID);
            mCache = new PersistentStore("Logger.RestService.cache." + gameID);
        }
        
        /** Makes an asynchronous HTTP request at the server, with custom authentication data. */
        private function requestToServer(method:String, path:String, data:Object, 
                                                   onComplete:Function, onError:Function, jsonSupport:Boolean = true):void
        {
            if (method == HttpMethod.GET && data)
            {
                path += "?" + encodeForUri(data);
                data = null;
            }
            
            var cachedResult:Object = null;
            var headers:Object = {};
            
            if (method == HttpMethod.GET && mCache.containsKey(path))
            {
                cachedResult = mCache.getObject(path);
                if (cachedResult) headers["If-None-Match"] = mCache.getMetaData(path, "eTag");
            }
            
            if (mAlwaysFail)
            {
                setTimeout(execute, 1, onError, "forced failure", 0, cachedResult);
                return;
            }

            var loader:URLLoader = new URLLoader();
            loader.addEventListener(Event.COMPLETE, onLoaderComplete);
            loader.addEventListener(IOErrorEvent.IO_ERROR, onLoaderError);
            loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onLoaderHttpStatus);
            
            var httpStatus:int = -1;
            var request:URLRequest = new URLRequest(path);
            request.method = method;
			
			if (method == HttpMethod.POST) // Sending JSON data in POST request
			{
				if (jsonSupport)
				{
					request.data = JSON.stringify(data);
					var headerObj:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
					request.requestHeaders = [headerObj];
				}
				else
				{
					var urlVars:URLVariables = new URLVariables();
					
					for (var key:String in data)
					{
						urlVars[key] = data[key];
					}
					
					request.data = urlVars;
				}
			}
			
            loader.load(request);
            
            function onLoaderComplete(event:Event):void
            {
                closeLoader();
                
                if (httpStatus != HttpStatus.OK)
                {
                    execute(onError, "Logging Server unreachable", httpStatus, cachedResult);
                }
                else
                {
					execute(onComplete);
					
					return;
                }
            }
            
            function onLoaderError(event:IOErrorEvent):void
            {
                closeLoader();
                execute(onError, "IO " + event.text, httpStatus, cachedResult);
            }
            
            function onLoaderHttpStatus(event:HTTPStatusEvent):void
            {
                httpStatus = event.status;
            }
            
            function closeLoader():void
            {
                loader.removeEventListener(Event.COMPLETE, onLoaderComplete);
                loader.removeEventListener(IOErrorEvent.IO_ERROR, onLoaderError);
                loader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onLoaderHttpStatus);
                loader.close();
            }
        }
        
        
        /** Adds an asynchronous HTTP request to a queue and immediately starts to process the
         *  queue. */
        public function requestQueued(method:String, path:String, data:Object=null, jsonSupport:Boolean = true):void
        {
            var queueLength:int;
            var metaData:String = null;
            
            if (method == HttpMethod.PUT)
            {
                // To allow developers to use Flox offline, we're optimistic here:
                // even though the operation might fail, we're saving the object in the cache.
                mCache.setObject(path, data);
                
                // if PUT is called repeatedly for the same resource (path),
                // we only need to keep the newest one.
                metaData = "PUT#" + path;
                queueLength = mQueue.length;
                
                mQueue.filter(function(i:int, m:String):Boolean
                {
                    if (i == queueLength-1) return true; // last element might be processed already
                    else return metaData != m;
                });
            }

            var request:Object = { method: method, path: path, data: data, jsonSupport: jsonSupport};
            mQueue.enqueue(request, metaData);
            processQueue();
        }
        
        /** Processes the request queue, executing requests in the order they were recorded.
         *  If the server cannot be reached, processing stops and is retried later; if a request
         *  produces an error, it is discarded. 
         *  @returns true if the queue is currently being processed. */
        public function processQueue():Boolean
        {
            if (!mProcessingQueue)
            {
                var element:Object = mQueue.peek();
                
                if (element != null)
                {
                    mProcessingQueue = true;
                    requestToServer(element.method, element.path, element.data, 
                                              onRequestComplete, onRequestError, element.jsonSupport);
                }
                else 
                {
                    mProcessingQueue = false;
                    dispatchEvent(new QueueEvent(QueueEvent.QUEUE_PROCESSED));
                }
            }
            
            return mProcessingQueue;
            
            function onRequestComplete(body:Object = null, httpStatus:int = 1):void
            {
                mProcessingQueue = false;
                mQueue.dequeue();
                processQueue();
            }
            
            function onRequestError(error:String, httpStatus:int):void
            {
                mProcessingQueue = false;
                
                if (HttpStatus.isTransientError(httpStatus))
                {
                    // server did not answer or is not available! we stop queue processing.
                    trace("Logging Server not reachable (device probably offline). " + 
                                 "HttpStatus: {0}", httpStatus);
                    dispatchEvent(new QueueEvent(QueueEvent.QUEUE_PROCESSED, httpStatus, error));
                }
                else
                {
                    // server answered, but there was a logic error -> no retry
                    trace("Logging service queue request failed: {0}, HttpStatus: {1}", 
                                    error, httpStatus);
                    
                    mQueue.dequeue();
                    processQueue();
                }
            }
        }
        
        /** Saves request queue and cache index to the disk. */
        public function flush():void
        {
            mQueue.flush();
            mCache.flush();
        }
        
        /** Clears the persistent queue. */
        public function clearQueue():void
        {
            mQueue.clear();
        }
        
        /** Clears the persistent cache. */
        public function clearCache():void
        {
            mCache.clear();
        }
        
        /** Returns an object that was previously received with a GET method from the cache.
         *  If 'data' is given, it is URL-encoded and added to the path.
         *  If 'eTag' is given, it must match the object's eTag; otherwise,
         *  the method returns null. */
        public function getFromCache(path:String, data:Object=null, eTag:String=null):Object
        {
            if (data) path += "?" + encodeForUri(data);
            if (mCache.containsKey(path))
            {
                var cachedObject:Object = mCache.getObject(path);
                var cachedETag:String = mCache.getMetaData(path, "eTag") as String;
                
                if (eTag == null || eTag == cachedETag)
                    return cachedObject;
            }
            return null;
        }
        
        // object encoding
        
        /** Encodes an object as parameters for a 'GET' request. */
        private static function encodeForUri(object:Object):String
        {
            var urlVariables:URLVariables = new URLVariables();
            for (var key:String in object) urlVariables[key] = object[key];
            return urlVariables.toString();
        }
        
        /** Encodes an object in JSON format, compresses it and returns its Base64 representation. */
        private static function encode(object:Object):String
        {
            sBuffer.writeUTFBytes(JSON.stringify(object, null, 0));
            sBuffer.compress();
            
            var encodedData:String = Base64.encodeByteArray(sBuffer);
            sBuffer.length = 0;
            
            return encodedData;
        }
        
        /** Decodes an object from JSON format, compressed in a Base64-encoded, zlib-compressed
         *  String. */
        private static function decode(string:String):Object
        {
            if (string == null || string == "") return null;
            
            Base64.decodeToByteArray(string, sBuffer);
            sBuffer.uncompress();
            
            var json:String = sBuffer.readUTFBytes(sBuffer.length);
            sBuffer.length = 0;
            
            return JSON.parse(json);
        }
        
        // properties
        
        /** @private 
         *  If enabled, all requests will fail. Useful only for unit testing. */
        internal function get alwaysFail():Boolean { return mAlwaysFail; }
        internal function set alwaysFail(value:Boolean):void { mAlwaysFail = value; }
        
        /** Indicates if the connection should be encryped using SSL/TLS. */
        public function get useSecureConnection():Boolean
        {
            return mUrl.toLowerCase().indexOf("https") == 0;
        }
        
        public function set useSecureConnection(value:Boolean):void
        {
            mUrl = mUrl.replace(/^http[s]?/, value ? "https" : "http");
        }
    }
}