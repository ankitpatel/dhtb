package com.adobe.nativeExtensions 
{
	/**
	 * ...
	 * @author ...
	 */
	public class VibrationHelper 
	{
		static private var vib:Vibration;
		
		public static function addVibration(duration:Number= 500):void
		{
			if (Vibration.isSupported)
			{
				if (!vib)
				{
					vib = new Vibration;	
				}
				vib.vibrate(duration);
			}
		}
	}

}